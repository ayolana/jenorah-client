@extends('layouts.app')

@section('content')

<style>
    .bs-wizard {margin-top: 40px;}

/*Form Wizard*/
.bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
.bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
.bs-wizard > .bs-wizard-step + .bs-wizard-step {}
.bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 16px; margin-bottom: 5px;}
.bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
.bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #fbe8aa; top: 45px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;} 
.bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: #fbbd19; border-radius: 50px; position: absolute; top: 8px; left: 8px; } 
.bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
.bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: #fbe8aa;}
.bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
.bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
.bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
.bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
.bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
.bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
.bs-wizard > .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
.bs-wizard > .bs-wizard-step:last-child  > .progress {width: 50%;}
.bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }
/*END Form Wizard*/

.asterisc {
  color: red;
}

</style>


<div class="header-breadcrumb mbottom50">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="section-title fleft">My Account</h1>
                        <ul class="reset-list">
                            <li>
                                <p>Shop</p>
                            </li>
                            <li>/</li>
                            <li class="active">
                                <p>Login</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
    </div><!-- end header-breadcrumb -->
   


   <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">


                <!-- <div class="container"> -->
        
        
            <div class="row bs-wizard" style="border-bottom:0;">
                
                <div class="col-xs-3 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum">Couple Details </div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Add info about the couple</div>
                </div>
                
                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                  <div class="text-center bs-wizard-stepnum">Event Details</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Add info about the Event</div>
                </div>
                
                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                  <div class="text-center bs-wizard-stepnum">Upload Pictures</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Upload Picture/Gallery</div>
                </div>
                
                <div class="col-xs-3 bs-wizard-step active"><!-- active -->
                  <div class="text-center bs-wizard-stepnum">Complete</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center"> CHose and template and Enjoy!</div>
                </div>
            </div>



  <div id="content" class="ContDiv">
    <div class="container">
      <div class="row">
        <div class="col-sm-9">
          <div class="ms-laptop-template">
            <div class="ms-laptop-cont">
              <img src="{{ asset('partials/images/masterslider/laptop.png') }}" alt="" class="ms-laptop-bg" />
              <div class="ms-lt-slider-cont">
                <!-- masterslider -->
                <div class="master-slider ms-skin-default slider" id="masterslider">
                      @foreach($template->pics as $p)
                    <div class="ms-slide">
                        <img src="{{ asset('uploads/temps/'.$p->picture)}}" data-src="{{ asset('uploads/temps/'.$p->picture)}}" alt="" width="423" height="282"/>  
                    </div>
                    @endforeach   
                </div>


                <!-- end of masterslider -->
              </div>
            </div>
          
          <span style="margin-left:300px" >
          <a  href="{{ route('view-template', ['id'=>$template->id ]) }}" class="btn btn-primary"> Choose {{ $template->name }} </a>
          </span>
          <br><br>

          </div>
        </div>

        <div class="col-sm-3">
          <div class="sidebar5 mbottom50">
            <h3>Templates</h3>
            <ul class="reset-list">
              @foreach($templates as $template)
              <li>
                <a href="" onclick="showTemp({{ $template->id }});return false;">{{ $template->name }}</a>
              </li>
              @endforeach
            </ul>
          </div><!-- end sidebar5 -->
        </div>
      </div>
    </div>
  </div><!-- end content -->


  <div id="DumpCon"></div>
  
        
        
        
        
        
    <!-- </div> -->
<!-- </div> -->

                </div>
               
            </div>
        </div>
    </div><!-- end content -->

  <script type="text/javascript">
      $(window).load(function() {
        // Laptop Slider  
        // var slider = new MasterSlider();
        // slider.setup('masterslider' , {
        //   width:492,
        //   height:309,
        //   speed:20,
        //   preload:0,
        //   space:2,
        //   view:'mask',

        // });
        // slider.control('arrows'); 
        // slider.control('bullets' , {autohide:false  , dir:"v", align:"top"});
        // slider.control('bullets',{autohide:false});

        $('.slider').masterslider({
              width:492,
          height:309,
          speed:20,
          preload:0,
          space:2,
          view:'mask',
            // more options...
            controls : {
                arrows : {autohide:false},
                bullets : {}
                // more slider controls...
            }
        });

      });

      function showTemp(id){
        //e.preventDefault()
        console.log(id);

        // $('.ContDiv').html('loading......').slideUp();
        $('.ContDiv').slideUp('slow');




        var url = "{{ route('get-template') }}"
        var token = '{{ csrf_token() }}'


       $.ajax
        ({
            type: "POST",
            url: url,
            data: ({ rnd : Math.random() * 100000, _token:token, template_id:id }),
            success: function(response){
                 console.log(response)
                 $('.ContDiv').html(response).slideDown('slow');

                  $('.slider').masterslider({
                    width:492,
                    height:309,
                    speed:20,
                    preload:0,
                    space:2,
                    view:'mask',
                      // more options...
                      controls : {
                          arrows : {autohide:false},
                          bullets : {}
                          // more slider controls...
                      }
                  });

            }
        });

      }


</script>

@stop