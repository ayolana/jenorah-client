
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">    

  
    <title> Slider in Laptop | RoyalSlider</title>
  

    <meta content="Touch-enabled image gallery and content slider plugin, that focuses on providing great user experience on every desktop and mobile device." name="description">
    <meta name="keywords" content="content slider, gallery, plugin, jquery, banner rotator">
    <meta name="viewport" content="width = device-width, initial-scale = 1.0" />
    <meta name="author" content="Dmitry Semenov">

    
   
      
    
    <style>
      #slider-in-laptop {
  width: 70%;
  height: 200px;
  padding: 2% 15% 7%;
  background: none;
}
#slider-in-laptop .rsOverflow,
#slider-in-laptop .rsSlide,
#slider-in-laptop .rsVideoFrameHolder,
#slider-in-laptop .rsThumbs {
  background: #151515;
}
.imgBg {
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: auto;
}
#slider-in-laptop .rsBullets {
  bottom: -25px;
}
.laptopBg {
  position: relative;
  width: 100%;
  height: auto;
}
#slider-in-laptop .rsBullets {
    bottom: -30px;
}

    </style>
    
  </head>
  <body >
  <header  id="main-header" class="clearfix">  
    <div  class="page wrapper"> 
       <div class="col span_6">
         <a id="logo" href="/plugins/royal-slider">Royal&thinsp;Slider</a>
         <nav>
            <a 
                  class=""
                
            href="/plugins/royal-slider/templates">Templates<span class="templates-count">11</span></a>
            <a  
                  class=""
                
                href="/plugins/royal-slider/faq">FAQ</a>
            <a 
                  class=""
                 href="/plugins/royal-slider/wordpress">WordPress Version</a>
         </nav>
          <a id="buy-slider" href="/plugins/royal-slider/pricing">Get this slider plugin &#9656;</a>
       </div>
    </div>
  </header>
  <div  class="page wrapper main-wrapper">  
      <div class="row clearfix">
    <div id="page-navigation" class="col span_6"> 
         
        <div class="left page-nav-item"> 
        
                <a href="/plugins/royal-slider/gallery-vertical-fade/" title="Next Example: 
Gallery with Fade and Vertical Thumbs"> ← Gallery with Fade and Vertical Thumbs</a> 
        
        </div> 
         
          
        <div class="right page-nav-item"> 
                <a href="/plugins/royal-slider/gallery-with-deeplinking/" title="Previous Example: 
Gallery with Deep Linking">Gallery with Deep Linking → </a> 
        </div> 
         
        
	</div> 
</div>
<div class="row clearfix">
  <div class="col span_4 fwImage">
    <div class="royalSlider-preview laptopBg">
      <img src="{{ asset('partials/test/laptop.png') }}" class="imgBg" width="707" height="400" />
      <div id="slider-in-laptop" class="royalSlider rsDefaultInv">
        <img src="{{ asset('partials/test/polo1.png') }}" />
        <img src="{{ asset('partials/test/polo2.png') }}" />
        <img src="{{ asset('partials/test/polo3.png') }}" />
        <img src="{{ asset('partials/test/polo4.png') }}" />
        <img src="/plugins/royal-slider/img/full-width/medium/2.jpg" data-rsVideo="https://vimeo.com/45778774" />
        <img src="/plugins/royal-slider/img/full-width/medium/3.jpg" />
        <img src="/plugins/royal-slider/img/full-width/medium/4.jpg" />
      </div>
    </div>
  </div>

  <div class="col span_2">
      <h1>Slider in laptop</h1>
      <p>Simple variation of image slider with bullets is used, image behind scales with slider automatically.</p>
      <p>Laptop mockup by <a href="http://dribbble.com/mbarvian">Maxwell Barvian</a>, photography by <a href="http://www.flickr.com/photos/gilderic/">Gilderic</a>.</p>
      <p>Buttons controlled by API:</p>
      <button id="slider-prev">Prev</button><button id="slider-next">Next</button>
    </div>
</div>

  </div>
  <div class="wrapper page">
     <footer class="row clearfix" id="main-footer">
    <div class="col span_6">
      <nav><a href="http://dimsemenov.com/subscribe.html">Email Newsletter</a><a href="http://twitter.com/dimsemenov">Twitter</a><a href="/plugins/royal-slider/documentation">Documentation</a><a href="/plugins/royal-slider/pricing">Pricing</a></nav>
      <p class="copy">© <a href="http://dimsemenov.com">Dmitry Semenov</a></p>
    </div>
  </footer>
  
    <script>
      jQuery(document).ready(function($) {
  var rsi = $('#slider-in-laptop').royalSlider({
    autoHeight: false,
    arrowsNav: false,
    fadeinLoadedSlide: false,
    controlNavigationSpacing: 0,
    controlNavigation: 'bullets',
    imageScaleMode: 'fill',
    imageAlignCenter: true,
    loop: false,
    loopRewind: false,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    autoScaleSlider: true,  
    autoScaleSliderWidth: 486,     
    autoScaleSliderHeight: 315,

    /* size of all images http://help.dimsemenov.com/kb/royalslider-jquery-plugin-faq/adding-width-and-height-properties-to-images */
    imgWidth: 792,
    imgHeight: 479



  }).data('royalSlider');
  $('#slider-next').click(function() {
    rsi.next();
  });
  $('#slider-prev').click(function() {
    rsi.prev();
  });
});

    </script>
  

      
  </div>
  </body>
</html>
