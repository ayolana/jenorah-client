@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.js"></script>

<style>
    .bs-wizard {margin-top: 40px;}

/*Form Wizard*/
.bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
.bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
.bs-wizard > .bs-wizard-step + .bs-wizard-step {}
.bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 16px; margin-bottom: 5px;}
.bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
.bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #fbe8aa; top: 45px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;} 
.bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: #fbbd19; border-radius: 50px; position: absolute; top: 8px; left: 8px; } 
.bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
.bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: #fbe8aa;}
.bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
.bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
.bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
.bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
.bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
.bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
.bs-wizard > .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
.bs-wizard > .bs-wizard-step:last-child  > .progress {width: 50%;}
.bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }
/*END Form Wizard*/

.asterisc {
  color: red;
}

</style>


<div class="header-breadcrumb mbottom50">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="section-title fleft">My Account</h1>
                        <ul class="reset-list">
                            <li>
                                <p>Shop</p>
                            </li>
                            <li>/</li>
                            <li class="active">
                                <p>Login</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
    </div><!-- end header-breadcrumb -->
   


   <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">


                <!-- <div class="container"> -->
        
        
            <div class="row bs-wizard" style="border-bottom:0;">
                
                <div class="col-xs-3 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum">Couple Details </div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>
                
                <div class="col-xs-3 bs-wizard-step active"><!-- complete -->
                  <div class="text-center bs-wizard-stepnum">Event Details</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Nam mollis tristique erat vel tristique. Aliquam erat volutpat. Mauris et vestibulum nisi. Duis molestie nisl sed scelerisque vestibulum. Nam placerat tristique placerat</div>
                </div>
                
                <div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->
                  <div class="text-center bs-wizard-stepnum">Upload Pictures</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Integer semper dolor ac auctor rutrum. Duis porta ipsum vitae mi bibendum bibendum</div>
                </div>
                
                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
                  <div class="text-center bs-wizard-stepnum">Complete</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center"> Curabitur mollis magna at blandit vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae</div>
                </div>
            </div>



            <div class="col-sm-12 mbottom50">
                    <div class="register-panel thin-shadow clearfix">
                        <div class="reg-panel-title">
                            <b>Fill all required fields</b>
                            @if (count($errors) > 0)
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li style="color:red">{{ $error }}</li>
                                        @endforeach
                                    </ul>
                            @endif
                        </div>
                        <form action="" method="post">
                            
                            {!! csrf_field() !!}

                            <div class="form-row">
                               
                                <div class="form-line required full">
                                    <label class="mbottom5" for="login_username">Marriage Date <span class="asterisc">*</span></label>
                                    <input type="text" id="login_username" name="marraige_date" class="datepicker"  required="required">
                                </div><!-- end form-line -->

                                
                                <hr>
                                <h3>Our Wedding Schedule <small>List of all wedding activities</small></h3>
                                 <table>
                                     <tr>
                                         <td>
                                             <div class="form-line required full">
                                                <label class="mbottom5" for="login_username">Name <span class="asterisc">*</span></label>
                                                <input type="text" id="login_username" name="engagement_name"  value="Engagement">
                                            </div><!-- end form-line -->
                                         </td>
                                         <td>
                                             <div class="form-line required full">
                                                <label class="mbottom5" for="login_username">Date <span class="asterisc">*</span></label>
                                                <input type="text" name="engagement_date" class="datepicker">
                                             </div>
                                         </td>
                                         <td>
                                             <div class="form-line required full">
                                                <label class="mbottom5" for="login_username">Time <span class="asterisc">*</span></label>
                                             <input type="text" name="engagement_time" placeholder="8am">
                                             </div>
                                         </td>
                                         <td>
                                             <div class="form-line required full">
                                                <label class="mbottom5" for="login_username">Info <span class="asterisc">*</span></label>
                                                <textarea class="form-control" name="engagement_info" id="" cols="50" rows="5">{{ old('our_love_story') }}</textarea>
                                             </div>
                                         </td>
                                     </tr>

                                     
                                      <tr>
                                         <td>
                                             <div class="form-line required full">
                                                <label class="mbottom5" for="login_username">Name <span class="asterisc">*</span></label>
                                                <input type="text" id="login_username" name="wedding_name"  value="Wedding">
                                            </div><!-- end form-line -->
                                         </td>
                                         <td>
                                             <div class="form-line required full">
                                                <label class="mbottom5" for="login_username">Date <span class="asterisc">*</span></label>
                                                <input type="text" name="wedding_date" class="datepicker">
                                             </div>
                                         </td>
                                         <td>
                                             <div class="form-line required full">
                                                <label class="mbottom5" for="login_username">Time <span class="asterisc">*</span></label>
                                             <input type="text" name="wedding_time" placeholder="12pm">
                                             </div>
                                         </td>
                                         <td>
                                             <div class="form-line required full">
                                                <label class="mbottom5" for="login_username">Info <span class="asterisc">*</span></label>
                                                <textarea class="form-control" name="wedding_info" id="" cols="50" rows="5">{{ old('our_love_story') }}</textarea>
                                             </div>
                                         </td>
                                     </tr>

                                      <tr>
                                         <td>
                                             <div class="form-line required full">
                                                <label class="mbottom5" for="login_username">Name <span class="asterisc">*</span></label>
                                                <input type="text" id="login_username" name="reception_name"  value="Reception">
                                            </div><!-- end form-line -->
                                         </td>
                                         <td>
                                             <div class="form-line required full">
                                                <label class="mbottom5" for="login_username">Date <span class="asterisc">*</span></label>
                                                <input type="text" name="reception_date" class="datepicker">
                                             </div>
                                         </td>
                                         <td>
                                             <div class="form-line required full">
                                                <label class="mbottom5" for="login_username">Time <span class="asterisc">*</span></label>
                                             <input type="text" name="reception_time" placeholder="3pm">
                                             </div>
                                         </td>
                                         <td>
                                             <div class="form-line required full">
                                                <label class="mbottom5" for="login_username">Info <span class="asterisc">*</span></label>
                                                <textarea class="form-control" name="reception_info" id="" cols="50" rows="5">{{ old('our_love_story') }}</textarea>
                                             </div>
                                         </td>
                                     </tr>
                                 </table>

                                
            

                                <div class="form-line full">
                                    <button class="btn btn-default mright15" type="submit">Proceed</button>
                                </div><!-- end form-line -->


                            </div><!-- end form-row -->
                        </form>
                    </div><!-- end register-panel -->
                </div>
        
        
        
        
        
    <!-- </div> -->
<!-- </div> -->

                </div>
               
            </div>
        </div>
    </div><!-- end content -->

    <script>
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    </script>

@stop