<div id="content">

    <div class="container">
      <div class="row">
        <div class="col-sm-9">
          <div class="ms-laptop-template">
            <div class="ms-laptop-cont">
              <img src="{{ asset('partials/images/masterslider/laptop.png') }}" alt="" class="ms-laptop-bg" />
              <div class="ms-lt-slider-cont">
                <!-- masterslider -->
                <div class="master-slider ms-skin-default slider" id="masterslider">
                    @foreach($template->pics as $p)
                    <div class="ms-slide">
                        <img src="{{ asset('uploads/temps/'.$p->picture)}}" data-src="{{ asset('uploads/temps/'.$p->picture)}}" alt="" width="423" height="282"/>  
                    </div>
                    @endforeach

                    <!-- {{ $template->name }} -->
                </div>
                <!-- end of masterslider -->
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="sidebar5 mbottom50">
            <h3> <small>switch</small> Templates</h3>
            <ul class="reset-list">
              @foreach($templates as $t)
              <li>
                <a href="" onclick="showTemp({{ $t->id }});return false;">{{ $t->name }}</a>
              </li>
              @endforeach
            </ul>
          </div><!-- end sidebar5 -->
        </div>
      </div>
       <span style="margin-left:300px" >
          <a  href="{{ route('view-template', ['id'=>$template->id ]) }}" class="btn btn-primary"> Choose {{ $template->name }} </a>
          </span>
          <br><br>
    </div>
  </div><!-- end content -->


