@extends('layouts.app')

@section('content')

<!-- {{asset('partials/images/apy_cople.png')}} -->
	<div id="content">
		<div class="mbottom50" id="product-launch-header">
			<div id="section-overlay">
			</div>
			<div class="container">
				<div class="row">
					<div class="col-sm-12 center">
						<h2>We provide the basic or full packages for various durations .</h2>
						<p>An online platform for clients, couples or individuals to create and customize their own wedding or event website from our various luxurious and classy background designs</p>
						<a class="btn btn-default btn-default2 mright25" href="{{ url('auth/register') }}">Get Started</a>
						<!-- <a class="watch-now animate3" href="#"><span class="icon-play mright10"></span>Watch now</a> -->
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="subscribe" class="center mbottom80">
						<h2 class="section-title mbottom5"><span class="tcolor">Jenorah</span> Wedding Planner</h2>
						<h5>Our Guest Experience, and thus, Client Satisfaction is the most important element of our events. We create bespoke design experiences, and so, no two weddings are the same</h5>
						<!-- <form action="http://lorthemes.us8.list-manage1.com/subscribe/post-json?u=42795504a248e486766e563b9&amp;id=f766078320&amp;c=?" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
							<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="" required>
							<div style="position: absolute; left: -5000px;">
	                            <input type="text" name="b_9ed253451c5b914e9c34e388b_6e3ba686ba" value="">
	                        </div>
							<button class=" btn-sm btn-small" type="submit" name="subscribe" id="mc-embedded-subscribe"><span class="icon-envelope"></span></button>
						</form>	 -->
						<div id="notification_container"></div>				
					</div>
				</div>				
			</div>
		</div><!-- end subscribe-->


		<div id="amazing">
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mbottom25 amazing-left">
						<h2 class="section-title2 right">We are just amazing!</h2>
						<!-- <p class="right">Aliquam accumsan mattis diam, eu dapibus purus tristique convallis.</p> -->
					</div>
					<div class="col-sm-6">
						<img src="{{ asset('partials/images/polo1.png')}}" alt="" class="img-responsive" />
					</div>
					<div class="col-sm-3 amazing-right">
						<ul class="number-list reset-list">
							<li>
								<span class="icon-number"></span>
								<h4>Select Your Design</h4>
							</li>
							<li>
								<span class="icon-number2"></span>
								<h4>Add Your Details</h4>
							</li>
							<li>
								<span class="icon-number3"></span>
								<h4>Share and Enjoy</h4>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div><!-- end amazing section-->
		
		
	   
	</div><!-- end content -->

@stop