<header id="header" class="nav-down header3">
		<div id="panel">
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<ul class="info-left reset-list">
							<li><span class="icon-location"></span><p>102580 Santa Monica BLVD Los Angeles</p></li>
							<li><span class="icon-phone"></span><p>080 3 321 8572</p></li>
							<li><span class="icon-envelope2"></span><p>info@jenorahcompany.com</p></li>
							<li><span class="icon-skype"></span><p>ayolana</p></li>
						</ul>
					</div>
					<div class="col-sm-3">
						<ul class="info-right reset-list">
							<li class="ico-twitter animation">
								<a href="#">
									<span class="icon-twitter"></span>
								</a>
							</li>
							<li class="ico-facebook animation">
								<a href="#">
									<span class="icon-facebook"></span>
								</a>
							</li>
							<li class="ico-pinterest animation">
								<a href="#">
									<span class="icon-pinterest"></span>
								</a>
							</li>
							<li class="ico-instagram animation">
								<a href="#">
									<span class="icon-instagram"></span>
								</a>
							</li>
							<li class="ico-dribbble animation">
								<a href="#">
									<span class="icon-dribbble"></span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="slide">
			<div class="container relative">
				<a href="#" class="btn-slide"><span class="icon-angle-up"></span></a>
			</div>
		</div><!-- end sliding panel -->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="relative onlyh3">
						<div class="logo-container hasinfocard">
							<h1 id="logo">
								<a href="index.html">
									<div class="logo-default">
										<img src="{{  asset('partials/images/jen_logo.fw.png') }}" width="110px"  alt="CleanLab HTML Template" title="Click to return to CleanLab Template's homepage" class="img-responsive">
									</div>
									<div class="logo-white">
										<img src="{{ asset('images/logo-white.png') }}" alt="CleanLab HTML Template" title="Click to return to CleanLab Template's homepage" class="img-responsive">
									</div>
								</a>
							</h1><!-- end logo -->
							<div id="infocard">
								<div class="row">
									<div class="col-sm-5 center">
										<img src="images/info-logo.png" alt="" class="mbottom20" />
										<h2>Jenorah wedding is a perfect place to get nice templates for your wedding</h2>
									</div>
									<div class="col-sm-7">
										<p><span class="icon-phone"></span>+234 803 826 1013</p>
										<p class="mbottom10"><span class="icon-envelope2 mright5"></span>Email: admin@jenorahcompany.com</p>
										<p>JenorahComany</p>
										<p class="mbottom30">10, Aribiya, Ota OLagos</p>
										<ul class="info-social center reset-list">
											<li class="width100 mbottom10">
												<p>Get Social</p>
											</li>
											<li>
												<a href="#">	
													<span class="icon-facebook"></span>
												</a>
											</li>
											<li>
												<a href="#">	
													<span class="icon-twitter"></span>
												</a>
											</li>
											<li>
												<a href="#">	
													<span class="icon-dribbble"></span>
												</a>
											</li>
											<li>
												<a href="#">
													<span class="icon-pinterest"></span>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div><!-- end infocard -->
						</div><!-- end logo-container -->
						
						<!-- Responsive Menu -->
						<div class="zn-res-menuwrapper">
							<a class="zn-res-trigger" href="#"></a>
						</div>
						<div id="main-menu">
							<ul>
							    <li class="has-sub mega active">
							    	<a href="index.html">Home</a>
							   		
							    </li>
							     <li class="has-sub">
									<a href="#">Designs</a>
									
								</li>
							    <li class="has-sub">
									<a href="#">Features</a>
									
								</li>
							    <li class="has-sub">
							        <a href="#">Pricing</a>
							        
							    </li>
							  <!--   <li>
								    <a href="#">Blog</a>
								    
							    </li> -->
							    <li class="has-sub mega">
							   	    <a href="{{ url('auth/register') }}">Get Started</a>
							   	    
							    </li>
							    <!-- <li class="has-sub">
							   	    <a href="#">Shop</a>
							   	    <ul>
							   	   	    <li>
							   			 	<a href="shop-home.html">Home Page</a>
							   		    <li>
							   		    <li>
						  		   	   	    <a href="product.html">Product Item</a>
						  		   	    </li>
							   	   	    <li> 
						  		   	   	    <a href="shopping-cart.html">Shopping Cart</a>
						  		   	    </li>
						  		   	    <li> 
						  		   	   	    <a href="shop-order-recieved.html">Order Confirmation</a>
						  		   	    </li>
						  		   	    <li> 
						  		   	   	    <a href="shop-account-login.html">My Account - Login</a>
						  		   	    </li>
							   	    </ul>
						        </li> -->
						       
							    <li>
							   	    <a href="#">Contact</a>
							   	    
							   </li>
							</ul>
						</div><!-- end main menu -->
					</div>
				</div>
			</div>
		</div>
	</header>