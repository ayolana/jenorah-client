 <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="#">JENORAH</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="{{ asset('admin/assets/images/users/avatar.jpg') }}" alt="JohnU Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcS05ASAVpO1V93V8ZJD7Fz7WWcwlpEPlz_vxPCvJk3pf8MCNR4GtA" alt="Johns Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">John Doe</div>
                                <div class="profile-data-title">Web Developer/Designer</div>
                            </div>
                            <div class="profile-controls">
                                <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                            </div>
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Navigation</li>
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-dashboard"></span> <span class="xn-text">Dashboards</span></a>
                        <ul>
                           <!--  <li class="active"><a href="index.html"><span class="xn-text">Dashboard 1</span></a></li>
                            <li><a href="dashboard.html"><span class="xn-text">Dashboard 2</span></a><div class="informer informer-danger">New!</div></li>
                            <li><a href="dashboard-dark.html"><span class="xn-text">Dashboard 3</span></a><div class="informer informer-danger">New!</div></li> -->
                        </ul>
                    </li>                    
                   

                   
                    <!-- <li class="xn-title">Components</li> -->
                                    
                   
                                   
                    <li>
                        <a href="maps.html"><span class="fa fa-map-marker"></span> <span class="xn-text">Maps</span></a>
                    </li>  

                    <li>
                        <a href="#"><span class="fa fa-map-marker"></span> <span class="xn-text">Details</span></a>
                    </li>                    
                  
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->