@extends('layouts.admin.admin')

@section('content')

  <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Forms Stuff</a></li>
                    <li class="active">Form Wizards</li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Form Wizards</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                          

                            <!-- START WIZARD WITH VALIDATION -->
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h3>Wizard with form validation</h3>                                
                                    <form action="{{ route('basic-details') }}" role="form" class="form-horizontal" method="post" id="wizard-validation">
                                   {!! csrf_field() !!}
                                    <div class="wizard show-submit wizard-validation">
                                        <ul>
                                            <li>
                                                <a href="#step-7">
                                                    <span class="stepNumber">1</span>
                                                    <span class="stepDesc">Login<br /><small>Information</small></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#step-8">
                                                    <span class="stepNumber">2</span>
                                                    <span class="stepDesc">User<br /><small>Personal data</small></span>
                                                </a>
                                            </li>                                    
                                        </ul>

                                        <div id="step-7">   

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Login</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control" name="login" placeholder="Login"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Password</label>
                                                <div class="col-md-10">
                                                    <input type="password" class="form-control" name="password" placeholder="Password" id="password"/>
                                                </div>
                                            </div>             
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Re-Password</label>
                                                <div class="col-md-10">
                                                    <input type="password" class="form-control" name="repassword" placeholder="Re-Password"/>
                                                </div>
                                            </div>

                                        </div>

                                        <div id="step-8">

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Name</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control" name="name" placeholder="Name"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">E-mail</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control" name="email" placeholder="Your email"/>
                                                </div>
                                            </div>                                    
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Adress</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control" name="adress" placeholder="Your adress"/>
                                                </div>                                        
                                            </div>                                                     

                                        </div>                                                                                                            
                                    </div>
                                    </form>
                                </div>
                            </div>                        
                            <!-- END WIZARD WITH VALIDATION -->

                        </div>

                    </div>
                    
                </div>
                <!-- PAGE CONTENT WRAPPER -->


@stop