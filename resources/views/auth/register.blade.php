@extends('layouts.app')

@section('content')


<div class="header-breadcrumb mbottom50">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="section-title fleft">My Account</h1>
                        <ul class="reset-list">
                            <li>
                                <p>Shop</p>
                            </li>
                            <li>/</li>
                            <li class="active">
                                <p>Registration Page</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
    </div><!-- end header-breadcrumb -->
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 mbottom50">

                 <div class="reg-panel-title">
                        <b>Login</b>
                    </div>
                    <p class="mbottom20">Creating an account is quick and easy, and will allow you to move through our checkout quicker. You can also store multiple shipping addresses, gain access to your order history, and much more. Go on.</p>
                    <a href="{{ url('auth/login') }}" class="btn2 btn-2 small-2d-slate animation">Login</a>
                   
                </div>
                <div class="col-sm-6 mtop40 mbottom50">

                 <div class="register-panel thin-shadow clearfix">
                        <div class="reg-panel-title">
                            <b>USER DETAILS</b>
                        </div>
                        <a href="#"><img src="{{ asset('partials/facebooklogin.png') }}" alt="" height="70px" width="300px"> </a>
                         @if (count($errors) > 0)
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li class="alert-danger">{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <form action="{{ url('auth/register') }}" method="post">

                        {!! csrf_field() !!}
                            <div class="form-row">
                                <div class="form-line full">
                                    <label for="login_username" class="mbottom5">Name</label>
                                    <input type="text" name="name" id="login_username">
                                </div><!-- end form-line -->
                                
                                <div class="form-line full">
                                    <label for="login_username" class="mbottom5">Email</label>
                                    <input type="email" name="email" id="login_username">
                                </div><!-- end form-line -->

                                <div class="form-line full">
                                    <label for="login_password" class="mbottom5">Phone</label>
                                    <input type="text" name="phone" id="login_password">
                                </div><!-- end form-line -->

                                <div class="form-line full">
                                    <label for="login_password" class="mbottom5">Password</label>
                                    <input type="password" name="password" id="login_password">
                                </div><!-- end form-line -->

                                  <div class="form-line full">
                                    <label for="login_password" class="mbottom5">Password Again</label>
                                    <input type="password" name="password_confirmation" id="login_password">
                                </div><!-- end form-line -->

                                <div class="form-line full">
                                    <button type="submit" class="btn btn-default mright15">Create</button>
                                    <a href="#" class="lost-password">Lost Password?</a>
                                </div><!-- end form-line -->

                                  

                            </div><!-- end form-row -->
                        </form>
                    </div><!-- end register-panel -->


                   
                </div>
            </div>
        </div>
    </div><!-- end content -->


@stop