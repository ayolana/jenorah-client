
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


    <link rel="shortcut icon" href="http://www.inspirothemes.com/polo-v2/images/favicon.png">
    <title>{{ ucwords($couple->slug) }} | Jenorah Client Website</title>

    <!-- Bootstrap Core CSS -->
    <link href="http://www.inspirothemes.com/polo-v2/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://www.inspirothemes.com/polo-v2/vendor/fontawesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <!-- <link href="http://www.inspirothemes.com/polo-v2/vendor/animateit/animate.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="{{ asset('templates/polo/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/polo/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/polo/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/polo/css/theme-base.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/polo/css/theme-elements.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/polo/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/polo/css/color-variations/pink.css') }}">
    
    <link href="{{ asset('templates/ilovethumbnail-slider.css') }}" rel="stylesheet" type="text/css" />


    <!-- Template color -->
    <!-- <link href="http://www.inspirothemes.com/polo-v2/css/color-variations/pink.css" rel="stylesheet" type="text/css" media="screen" title="pink"> -->




    <!-- LOAD GOOGLE FONTS -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600%7CRaleway:100,300,600,700,800" rel="stylesheet" type="text/css" />


    <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet" type="text/css">

    <!-- CSS CUSTOM STYLE -->
    <link rel="stylesheet" href="{{ asset('templates/polo/wedding-style.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/polo/css/custom.css') }}">

    <!-- <link rel="stylesheet" type="text/css" href="http://www.inspirothemes.com/polo-v2/homepages/wedding/css/wedding-style.css" media="screen" /> -->


    <!-- CSS CUSTOM STYLE -->
    <!-- <link rel="stylesheet" type="text/css" href="http://www.inspirothemes.com/polo-v2/css/custom.css" media="screen" /> -->

    <!--VENDOR SCRIPT-->
    <script src="{{ asset('templates/polo/jquery/jquery-1.11.2.min.js')}}"></script>
    <script src="{{ asset('templates/polo/jquery/plugins-compressed.js')}}"></script>


</head>

<body class="wide" style="background-image: url(images/pattern/29.png);">
    

    <!-- WRAPPER -->
    <div class="wrapper">

        <!-- SLIDESHOW SLIDER -->
        <section id="vegas-slideshow" class="p-t-200 p-b-200">
            <div class="container">
                <div class="circle-2">
                    <div class="vertical-align">
                        <h3 class="text-colored text-medium">{{ $couple->bride->display_name }} & {{ $couple->groom->display_name }}</h3>
                        @if(time() < strtotime($couple->marriage_date))
                        <h4>~ Are getting Married on ~ </h4>
                        <div class="countdown small" data-countdown="{{ date('Y/m/d', strtotime($couple->marriage_date)) }}"></div>
                        @else
                        <h4>~ Got Married on ~ </h4>
                        <div class="countdown small">{{ date('l, dS F Y', strtotime($couple->marriage_date)) }}</div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
        <!-- END: SLIDESHOW SLIDER -->

        <!--HEADER-->
        <header id="header" class="">
            <div id="header-wrap">
                <div class="container">

                    <!--MOBILE MENU -->
                    <div class="nav-main-menu-responsive">
                        <button class="lines-button x" type="button" data-toggle="collapse" data-target=".main-menu-collapse">
                            <span class="lines"></span>
                        </button>
                    </div>
                    <!--END: MOBILE MENU -->

                    <!--NAVIGATION-->
                    <div class="navbar-collapse collapse main-menu-collapse navigation-wrap">
                        <div class="container">
                            <nav class="main-menu mega-menu center" id="mainMenu">
                                <ul class="main-menu nav nav-pills">
                                    <li class="dropdown"><a href="#vegas-slideshow" class="scroll-to"><i class="fa fa-home"></i></a> </li>
                                    <li class="dropdown"><a href="#section2" class="scroll-to">The Couple</a> </li>
                                    <li class="dropdown"><a href="#section3" class="scroll-to">Gallery</a> </li>
                                    <li class="dropdown"><a href="#section4" class="scroll-to">Love Story</a> </li>
                                    <li class="dropdown"><a href="#section5" class="scroll-to">Events Schedule</a> </li>
                                    <li class="dropdown"><a href="#section6" class="scroll-to">Wedding Location</a> </li>
                                    <li class="dropdown"><a href="#section7" class="scroll-to">RSVP</a> </li>

                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!--END: NAVIGATION-->
                </div>
            </div>
        </header>
        <!--END: HEADER-->

        <!-- GROOM & BRIDE -->
        <section id="section2" class="background-image p-b-100" style="background-image:url(homepages/wedding/images/shape-background.png); background-position:center center;">
            <div class="container">

                <div class="row">
                    <div class="col-md-4" data-animation="fadeInLeft" data-animation-delay="300">
                        <div class="wedding-circle-image">
                            <img src="{{ env('picdisplay').$couple->bride->picture }}" alt="{{ $couple->bride->display_name }} pictr">
                        </div>
                        <div class="text-center">
                            <h2 class="text-colored">{{ $couple->bride->full_name }}</h2>
                            <p>{{ substr($couple->bride->about, 0, 200) }}</p>
                        </div>
                    </div>
                    <div class="col-md-4" data-animation="fadeInUp" data-animation-delay="100">

                        <div class="text-center">
                            <h2 class="text-colored text-large">The Couple</h2>
                            <p class="lead">
                                Meet the Bride & the Groom
                            </p>
                            <img src="{{ asset('templates/polo/homepages/wedding/images/rings.png')}}" class="img-responsive" alt="">
                        </div>
                    </div>
                    <div class="col-md-4" data-animation="fadeInRight" data-animation-delay="300">
                        <div class="wedding-circle-image">
                            <img src="{{ env('picdisplay').$couple->groom->picture }}" alt="{{ $couple->groom->display_name }} picture">
                        </div>
                        <div class="text-center">
                            <h2 class="text-colored">{{ $couple->groom->full_name }}</h2>
                            <p>{{ substr($couple->groom->about, 0, 200) }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END: GROOM & BRIDE -->

        <!-- GALLERY -->
        <section id="section3" class="p-t-0" style="background-image:url(homepages/wedding/images/shape-right-bottom.png); background-position:bottom right; background-repeat:no-repeat;">
            <div class="container">
                <div class="heading heading-colored heading-center">
                    <h2 class="m-t-40 m-b-80">Ceremony Gallery</h2>
                </div>
                
                 <div style="padding:0px 0;">
                <div id="thumbnail-slider">

                    <div class="inner">
                        <ul>
                        
                           @foreach($galleryPixs as $p)
                            <li> <a class="thumb" href="{{ env('picdisplay').$p }}"></a> </li>
                            @endforeach
                           
                        </ul>
                    </div>
                </div>
            </div>

               
            </div>
        </section>
        <!-- END: GALLERY -->

        <!-- TIMELINE -->
        <section id="section4" style="background-image:url(homepages/wedding/images/shape-left-top.png); background-position:top left; background-repeat:no-repeat;">
            <div class="container">
                <div class="heading heading-colored heading-center">
                    <h2 class="m-t-40 m-b-40">Our Love Story</h2>
                    <p class="lead">
                        {{ $couple->our_love_story }}
                    </p>
                </div>

                <div class="timeline timeline-colored">
                    <ul class="timeline-circles">
                        <li class="timeline-date">Our days</li>
                        <li>
                            <div class="timeline-block text-right">
                                <h2>Day we meet each other,<small> 10 Jun 2015</small></h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-block">
                                <h2>Our first kiss,<small> 10 Jun 2015</small></h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-block text-right">
                                <h2>Our first trip,<small> 10 Jun 2015</small></h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-block">
                                <h2>Our Engagement,<small> 10 Jun 2015</small></h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-block text-right">
                                <h2>Our Wedding Day,<small> 10 Jun 2015</small></h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- END: TIMELINE -->

        <!-- EVENTS -->
        <section id="section5" style="background-image:url(homepages/wedding/images/shape-right-bottom.png); background-position:bottom right; background-repeat:no-repeat;">
            <div class="container">
                <div class="heading heading-colored heading-center m-b-80">
                    <h2>Events Schedule</h2>
                    <p class="lead">
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                    </p>
                </div>

                <div class="row">
                    @foreach($couple->schedules as $e)
                        <div class="col-md-4 wedding-event">
                            <h3 class="text-colored m-b-0">{{ $e->name }}</h3>
                            <h5>{{ date('d, F Y', strtotime($e->date)) }} <small>{{ $e->time }}</small></h5>
                            <p>{{ $e->info }}</p>
                        </div>
                    @endforeach
                    
                   
                </div>
            </div>
        </section>
        <!-- END: EVENTS -->

        <!-- MAP -->
        <section id="section6" style="background-image:url(homepages/wedding/images/shape-left-top.png); background-position:top left; background-repeat:no-repeat;">
            <div class="container">
                <div class="heading heading-colored heading-center">
                    <h2 class="m-b-30">Our Wedding Location</h2>
                    <p class="lead text-colored">{{ $couple->location }}</p>
                </div>
                <script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false"></script>
                <div class="map" data-map-address="Melburne, Australia" data-map-type="TERRAIN" data-map-icon="images/markers/marker4.png" data-height-xxs="160"></div>
            </div>
        </section>
        <!-- END: MAP -->

        <!-- RESERVATION -->
        <section id="section7" style="background-image:url(homepages/wedding/images/shape-right-bottom.png); background-position:bottom right; background-repeat:no-repeat;">
            <div class="container">
                <div class="heading heading-colored heading-center m-b-20">
                    <h2>RSVP</h2>
                    <p class="lead">
                        Please don't forget to be present at the Wedding :)
                    </p>
                </div>
                <form id="widget-contact-form" action="{{ route('contact-us') }}" role="form" method="post" class="form-grey-fields text-center">
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-6 center">
                            <div class="form-group">
                                <input type="text" aria-required="true" name="name" class="form-control required name" placeholder="Enter your Name">
                            </div>
                            <div class="form-group">
                                <input type="email" aria-required="true" name="email" class="form-control required email" placeholder="Enter your Email">
                            </div>
                            <div class="form-group">
                                <input type="text" aria-required="true" name="phone" class="form-control required" placeholder="Enter your phone">
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="guests">
                                    <option value="Wedding Ceremony">Guests</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>

                                </select>
                            </div>
                    <input type="hidden" name="couple_id" value="{{ $couple->id }}"> 

                           <!--  <div class="form-group">
                                <select class="form-control" name="widget-contact-form-events">
                                    <option value="Wedding Ceremony">Wedding Ceremony</option>
                                    <option value="Reception">Reception</option>
                                    <option value="All Events">All Events</option>
                                    <option value="Not Attending">Not Attending</option>
                                </select>
                            </div> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group text-center">
                            <input type="text" class="hidden" id="widget-contact-form-antispam" name="widget-contact-form-antispam" value="" />
                            <button class="btn btn-primary" type="submit" id="form-submit"><i class="fa fa-paper-plane"></i>&nbsp;Submit</button>
                        </div>
                    </div>
                </form>
               <!-- <script type="text/javascript">
                    jQuery("#widget-contact-form").validate({

                        submitHandler: function(form) {

                            jQuery(form).ajaxSubmit({
                                success: function(text) {
                                    if (text.response == 'success') {
                                        $.notify({
                                            message: "We have <strong>successfully</strong> received your RVSP confirmation!"
                                        }, {
                                            type: 'success'
                                        });
                                        $(form)[0].reset();

                                    } else {
                                        $.notify({
                                            message: text.message
                                        }, {
                                            type: 'danger'
                                        });
                                    }
                                }
                            });
                        }
                    });

                </script>-->

            </div>
        </section>
        <!-- END: RESERVATION -->

        <!-- FOOTER -->
        <footer class="background-colored text-light" id="footer">
            <div class="footer-content">
                <div class="container">

                    <div class="row">
                        <div class="col-md-4">
                            <h4 class="widget-title">{{ $couple->bride->display_name }} {{ $couple->groom->display_name }}</h4>

                            <p style="margin-top: 12px;">{{ substr($couple->our_love_story, 0, 200) }}</p>
                        </div>

                        <div class="col-md-4">
                            <h4 class="widget-title">Follow us</h4>
                            <h2 class="m-t-0"><i class="fa fa-facebook-square"></i> <span>Facebook</span><br><i class="fa fa-twitter"></i> <span>Twitter</span></h2>


                        </div>

                        <div class="col-md-4">
                            <h4 class="widget-title">Contact us</h4>
                            <h2 class="m-b-0"><i class="fa fa-phone"></i><a href="tel:+1234567891011"> +1 10 987 654 3210</a></h2>
                            <h2 class="m-b-0"><i class="fa fa-fax"></i><a href="tel:+1234567891011"> +1 10 987 654 3210</a></h2>

                        </div>


                    </div>
                </div>
            </div>
            <div class="copyright-content">
                <div class="container">
                    <div class="row">




                        <div class="copyright-text text-center"> &copy; 2015 POLO - Best HTML5 Template Ever. All Rights Reserved. <a href="http://www.inspiro-media.com" target="_blank">INSPIRO</a>
                        </div>

                    </div>
                </div>
            </div>
        </footer>
        <!-- END: FOOTER -->

    </div>
    <!-- END: WRAPPER -->

    <!-- GO TOP BUTTON -->
    <a class="gototop gototop-button" href="#"><i class="fa fa-chevron-up"></i></a>

    <!--VEGAS SLIDESHOW-->
    <link rel="stylesheet" href="{{ asset('templates/polo/vegas.min.css') }}">
    <script src="{{ asset('templates/polo/jquery/vegas.min.js') }} "></script>


    <script>
        $(function() {
            $("#vegas-slideshow").vegas({
                delay: 4000,
                transition: 'fade2',
                transitionDuration: 1000,
                slides: [{
                    src: "{{ asset('templates/polo/homepages/wedding/images/2.jpg') }}"
                }, {
                    src: "{{ asset('templates/polo/homepages/wedding/images/3.jpg') }}"
                }, {
                    src: "{{ asset('templates/polo/homepages/wedding/images/1.jpg') }}"
                }]
            });
        });

    </script>

    <!-- Theme Base, Components and Settings -->
    <script src="{{ asset('templates/polo/jquery/theme-functions.js')}}"></script>
    <script src="{{ asset('templates/polo/jquery/custom.js')}}"></script>

    <script src="{{ asset('templates/ilovethumbnail-slider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin/jquery.form.js') }}" type="text/javascript"></script>

    <script>

        $('#widget-contact-form').ajaxForm({ 
                    headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
                    beforeSubmit:btn,
                    success:showResponse
            }); 

                    function btn(){
                        console.log('Logging things')
                    }

                    function showResponse(){
                        console.log('Am here for you.')
                        // $('#flash').html('Your comments was successfully submitted')
                         $.notify({
                                message: "We have <strong>successfully</strong> received your RVSP confirmation!"
                            }, {
                                type: 'success'
                            });

                        setTimeout(function(){
                            $('#flash').hide()
                        }, 3000);

                        $("#widget-contact-form")[0].reset()

                    }

    </script>


    <!-- Custom js file -->



</body>

</html>
