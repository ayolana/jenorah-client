
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ ucwords($couple->slug) }} | Jenorah Client Website</title>
    <!-- Bootstrap 3.2 -->
    <link rel="stylesheet" href="{{ asset('templates/ilove/css/bootstrap.min.css') }}">
    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
    <!-- Font Awesome 4.2.0 -->
	<link href="{{ asset('templates/ilove/css/font-awesome-4.2.0/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- ilove Font 1.0 -->
	<link href="{{ asset('templates/ilove/css/ilove/font-ilove.css') }}" rel="stylesheet">   
    <!-- Animate -->
	<link rel="stylesheet" href="{{ asset('templates/ilove/css/animate.css')}}">
    <!-- Magnific popup -->
	<link rel="stylesheet" href="{{ asset('templates/ilove/css/magnific-popup.css') }}">
    <!-- Owl stylesheet -->
    <link rel="stylesheet" href="{{ asset('templates/ilove/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{ asset('templates/ilove/css/owl.theme.css')}}">
    <!-- Vertical-nav -->
	<link rel="stylesheet" href="{{ asset('templates/ilove/css/vertical-nav.css') }}">    
    <!-- Custom Style -->
	<link rel="stylesheet" href="{{ asset('templates/ilove/css/style.css') }}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="{{ asset('templates/ilovethumbnail-slider.css') }}" rel="stylesheet" type="text/css" />

  </head>
  <body id="page-top" data-spy="scroll" data-target=".navbar-custom">
  	<!-- Preloader -->
    <div id="preloader">
        <div class="spinner">
          <div class="rect1"></div>
          <div class="rect2"></div>
          <div class="rect3"></div>
          <div class="rect4"></div>
          <div class="rect5"></div>
        </div>
    </div>
        
    <!--Navbar-->
    <nav id="cd-vertical-nav">
		<ul>
			<li class="page-scroll">
				<a href="#page-top" data-number="1">
					<span class="cd-dot"></span>
					<span class="cd-label">Top</span>
				</a>
			</li>
			<li class="page-scroll">
				<a href="#our-story" data-number="2">
					<span class="cd-dot"></span>
					<span class="cd-label">Our Story</span>
				</a>
			</li>
			<li class="page-scroll">
				<a href="#best-friends" data-number="3">
					<span class="cd-dot"></span>
					<span class="cd-label">Best Friends</span>
				</a>
			</li>
			<li class="page-scroll">
				<a href="#the-day" data-number="4">
					<span class="cd-dot"></span>
					<span class="cd-label">The Day</span>
				</a>
			</li>
			<li class="page-scroll">
				<a href="#rsvp" data-number="5">
					<span class="cd-dot"></span>
					<span class="cd-label">RSVP</span>
				</a>
			</li>
		</ul>
	</nav>
	<a class="cd-nav-trigger cd-img-replace">Open navigation<span></span></a>
    <div class="topbar-nav">        
        <nav class="navbar navbar-default navbar-custom" role="navigation">
        	<div class="container">        
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                        <i class="fa fa-bars"></i>
                    </button> 
                    <a class="navbar-brand" href="#page-top">
                        <img src="{{ asset('templates/ilove/images/logo.png') }}" alt="logo">
                    </a>                              
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                    <ul class="nav navbar-nav">
                        <!-- Hidden li included to remove active class from Adam & Eve link when scrolled up past about section -->
                        <li class="hidden">
                            <a href="#page-top"></a>
                        </li>                                               
                        <li class="page-scroll">
                            <a href="index.html#about">Adam & Eve</a>
                        </li>
                        <li class="page-scroll">
                            <a href="index.html#family">Our Family</a>
                        </li>                        
                        <li class="page-scroll dropdown selected">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#moments">Big Moments</a>
                            <ul class="dropdown-menu wow swing" role="menu">
                            	<li><a href="wedding.html">Wedding Event</a></li>
                                <li><a href="#">Birthday</a></li>
                                <li><a href="#">Graduation</a></li>
                            </ul>                             
                        </li>
                        <li class="page-scroll">
                            <a href="index.html#favourites">Favorite List</a>
                        </li>  
                        <li class="page-scroll dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#blog">Our Blog</a>
                            <ul class="dropdown-menu wow swing" role="menu">
                            	<li><a href="category.html">Category Page</a></li>
                                <li><a href="post.html">Single Post</a></li>
                                <li><a href="404.html">404 Error Page</a></li>
                            </ul>
                        </li>                                                          
                        <li class="page-scroll">
                            <a href="index.html#contactus">Contact</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            
        	</div>
        </nav>
	</div>     
    
    <!--Main Header-->
    <div class="overlay2"></div>
        @if(time() < strtotime($couple->marriage_date))
            <div class="wedding"> 
                <h2 class="slideInDown wow" data-wow-delay="0.5s">Getting Married in</h2>
                <div class="time" id="time"></div>
                <h4 class="fadeIn wow animated" data-wow-delay="1.2s">You Are Invited</h4>
                <div class="btn-special slideInUp wow animated page-scroll" data-wow-delay="0.7s">
                    <a href="#our-story"><span>Discover More</span></a>
                </div>  
            </div>
        @else
            <div class="wedding"> 
                <h2 class="slideInDown wow" data-wow-delay="0.5s">Got Married on</h2>
                <div class="time"> {{ date('l, dS F Y', strtotime($couple->marriage_date)) }} </div>

                <!-- <h4 class="fadeIn wow animated" data-wow-delay="1.2s">You Are Invited</h4> -->
                <div class="btn-special slideInUp wow animated page-scroll" data-wow-delay="0.7s">
                    <a href="#our-story"><span>Discover More</span></a>
                </div>  
            </div>
        @endif

    
    <section id="our-story">
        <div class="container">
        	<div class="row">
				<div class="col-md-12">
                	<div class="heading">
                        <h2 class="wow fadeInUp underline padtop50" data-wow-delay="0.5s">Our Story</h2>
                        <p class="wow fadeInUp" data-wow-delay="0.7s">
                        {{ $couple->our_love_story }}
                        </p>                        
                    </div>             
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <ul class="ch-grid man wow flipInX" data-wow-delay="0.5s"  >
                            <li>
                                <div class="ch-item ch-img-1" style="background:url( {{ env('picdisplay').$couple->groom->picture }} ) repeat;">
                                    <div class="ch-info">
                                        <h3>I'm {{ $couple->groom->display_name }}</h3>
                                        <p> {{ substr($couple->groom->about, 0, 200) }}</p>
                                    </div>
                                </div>
                            </li>
                        </ul>	
                    </div>
                    <div class="and">&amp;</div> 
                    <div class="col-md-6">
                        <ul class="ch-grid woman wow flipInX" data-wow-delay="0.5s">
                            <li>
                                <div class="ch-item ch-img-2" style="background:url( {{ env('picdisplay').$couple->bride->picture }} ) repeat;">
                                    <div class="ch-info">
                                        <h3>I'm {{ $couple->bride->display_name }}</h3>
                                        <p>{{ substr($couple->groom->about, 0, 200) }}</p>
                                    </div>
                                </div>
                            </li>
                        </ul>	
                    </div>              
            	</div>           
        	</div>
        </div>
        <div class="separator"></div>
    </section> 
    
    <section id="best-friends" class='collapse'>
        <div class="container">
        	<div class="row">
				<div class="col-md-12">
                	<div class="heading">
                        <h2 class="wow fadeInUp underline padtop50" data-wow-delay="0.5s">Groomsmen</h2>
                        <p class="wow fadeInUp" data-wow-delay="0.7s">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam rhoncus, leo at pretium commodo, dui ipsum tristique dolor, in ullamcorper tellus velit quis nisl. In placerat volutpat elit quis dictum.
                        </p>                        
                    </div>             
                </div>
                <div class="col-md-12 text-center">
                	<div class="bestfriends cs-style-3 wow fadeInUp" data-wow-delay="0.5s">
                        <a href="#">
                            <figure>
                                <img src="images/groomsmen-1.jpg" alt="friends" class="img-responsive">
                            </figure>
                        </a>
                        <h4><a href="#">Michael Page</a></h4>
                        <h5>Best Man</h5>
                    	<div class="social-icons">
                    	<ul>
                            <li class="twitter">
                            <a href="http://www.twitter.com" target="_blank">Twitter</a>
                            </li>
                            
                            <li class="facebook">
                            <a href="http://www.facebook.com" target="_blank">Facebook</a>
                            </li>
                            
                            <li class="youtube">
                            <a href="http://www.youtube.com" target="_blank">YouTube</a>
                            </li>
                            
                            <li class="googleplus">
                            <a href="http://www.plus.google.com" target="_blank">Google +r</a>
                            </li>
                            
                            <li class="pinterest">
                            <a href="http://www.pinterest.com/" target="_blank">Pinterest</a>
                            </li>                                                                                                                                              
                    	</ul>
                    </div>
                    </div>
                    
                    <div class="bestfriends cs-style-3 wow fadeInUp" data-wow-delay="0.5s">
                        <a href="#">
                            <figure>
                                <img src="images/groomsmen-2.jpg" alt="friends" class="img-responsive">
                            </figure>
                        </a>
                        <h4><a href="#">Bill Swan</a></h4>
                        <h5>Best Friend</h5>
                    	<div class="social-icons">
                    	<ul>
                            <li class="twitter">
                            <a href="http://www.twitter.com" target="_blank">Twitter</a>
                            </li>
                            
                            <li class="facebook">
                            <a href="http://www.facebook.com" target="_blank">Facebook</a>
                            </li>
                            
                            <li class="youtube">
                            <a href="http://www.youtube.com" target="_blank">YouTube</a>
                            </li>
                            
                            <li class="googleplus">
                            <a href="http://www.plus.google.com" target="_blank">Google +r</a>
                            </li>
                            
                            <li class="pinterest">
                            <a href="http://www.pinterest.com/" target="_blank">Pinterest</a>
                            </li>                                                                                                                                              
                    	</ul>
                    </div>
                    </div>
                    
                    <div class="bestfriends cs-style-3 wow fadeInUp" data-wow-delay="0.5s">
                        <a href="#">
                            <figure>
                                <img src="images/groomsmen-3.jpg" alt="friends" class="img-responsive">
                            </figure>
                        </a>
                        <h4><a href="#">Every Johns</a></h4>
                        <h5>College Friend</h5>
                    	<div class="social-icons">
                    	<ul>
                            <li class="twitter">
                            <a href="http://www.twitter.com" target="_blank">Twitter</a>
                            </li>
                            
                            <li class="facebook">
                            <a href="http://www.facebook.com" target="_blank">Facebook</a>
                            </li>
                            
                            <li class="youtube">
                            <a href="http://www.youtube.com" target="_blank">YouTube</a>
                            </li>
                            
                            <li class="googleplus">
                            <a href="http://www.plus.google.com" target="_blank">Google +r</a>
                            </li>
                            
                            <li class="pinterest">
                            <a href="http://www.pinterest.com/" target="_blank">Pinterest</a>
                            </li>                                                                                                                                              
                    	</ul>
                    </div>
                    </div>
                    
                    <div class="bestfriends cs-style-3 wow fadeInUp" data-wow-delay="0.5s">
                        <a href="#">
                            <figure>
                                <img src="images/groomsmen-4.jpg" alt="friends" class="img-responsive">
                            </figure>
                        </a>
                        <h4><a href="#">Martin Nos</a></h4>
                        <h5>Room Mate</h5>
                    	<div class="social-icons">
                    	<ul>
                            <li class="twitter">
                            <a href="http://www.twitter.com" target="_blank">Twitter</a>
                            </li>
                            
                            <li class="facebook">
                            <a href="http://www.facebook.com" target="_blank">Facebook</a>
                            </li>
                            
                            <li class="youtube">
                            <a href="http://www.youtube.com" target="_blank">YouTube</a>
                            </li>
                            
                            <li class="googleplus">
                            <a href="http://www.plus.google.com" target="_blank">Google +r</a>
                            </li>
                            
                            <li class="pinterest">
                            <a href="http://www.pinterest.com/" target="_blank">Pinterest</a>
                            </li>                                                                                                                                              
                    	</ul>
                    </div>
                    </div>
                </div>


				<div class="col-md-12">
                	<div class="heading">
                        <h2 class="wow fadeInUp underline padtop50" data-wow-delay="0.5s">Bridesmaids</h2>
                        <p class="wow fadeInUp" data-wow-delay="0.7s">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam rhoncus, leo at pretium commodo, dui ipsum tristique dolor, in ullamcorper tellus velit quis nisl. In placerat volutpat elit quis dictum.
                        </p>                        
                    </div>             
                </div>
                <div class="col-md-12 text-center">
                	<div class="bestfriends cs-style-3 wow fadeInUp" data-wow-delay="0.5s">
                        <a href="#">
                            <figure>
                                <img src="images/bridesmaids-1.jpg" alt="friends" class="img-responsive">
                            </figure>
                        </a>
                        <h4><a href="#">Lexy Swan</a></h4>
                        <h5>Bestmaid</h5>
                    	<div class="social-icons">
                    	<ul>
                            <li class="twitter">
                            <a href="http://www.twitter.com" target="_blank">Twitter</a>
                            </li>
                            
                            <li class="facebook">
                            <a href="http://www.facebook.com" target="_blank">Facebook</a>
                            </li>
                            
                            <li class="youtube">
                            <a href="http://www.youtube.com" target="_blank">YouTube</a>
                            </li>
                            
                            <li class="googleplus">
                            <a href="http://www.plus.google.com" target="_blank">Google +r</a>
                            </li>
                            
                            <li class="pinterest">
                            <a href="http://www.pinterest.com/" target="_blank">Pinterest</a>
                            </li>                                                                                                                                              
                    	</ul>
                    </div>
                    </div>
                    
                    <div class="bestfriends cs-style-3 wow fadeInUp" data-wow-delay="0.5s">
                        <a href="#">
                            <figure>
                                <img src="images/bridesmaids-2.jpg" alt="friends" class="img-responsive">
                            </figure>
                        </a>
                        <h4><a href="#">Marria Grace</a></h4>
                        <h5>Best Friend</h5>
                    	<div class="social-icons">
                    	<ul>
                            <li class="twitter">
                            <a href="http://www.twitter.com" target="_blank">Twitter</a>
                            </li>
                            
                            <li class="facebook">
                            <a href="http://www.facebook.com" target="_blank">Facebook</a>
                            </li>
                            
                            <li class="youtube">
                            <a href="http://www.youtube.com" target="_blank">YouTube</a>
                            </li>
                            
                            <li class="googleplus">
                            <a href="http://www.plus.google.com" target="_blank">Google +r</a>
                            </li>
                            
                            <li class="pinterest">
                            <a href="http://www.pinterest.com/" target="_blank">Pinterest</a>
                            </li>                                                                                                                                              
                    	</ul>
                    </div>
                    </div>
                    
                    <div class="bestfriends cs-style-3 wow fadeInUp" data-wow-delay="0.5s">
                        <a href="#">
                            <figure>
                                <img src="images/bridesmaids-3.jpg" alt="friends" class="img-responsive">
                            </figure>
                        </a>
                        <h4><a href="#">Hanna Martin</a></h4>
                        <h5>College Friend</h5>
                    	<div class="social-icons">
                    	<ul>
                            <li class="twitter">
                            <a href="http://www.twitter.com" target="_blank">Twitter</a>
                            </li>
                            
                            <li class="facebook">
                            <a href="http://www.facebook.com" target="_blank">Facebook</a>
                            </li>
                            
                            <li class="youtube">
                            <a href="http://www.youtube.com" target="_blank">YouTube</a>
                            </li>
                            
                            <li class="googleplus">
                            <a href="http://www.plus.google.com" target="_blank">Google +r</a>
                            </li>
                            
                            <li class="pinterest">
                            <a href="http://www.pinterest.com/" target="_blank">Pinterest</a>
                            </li>                                                                                                                                              
                    	</ul>
                    </div>
                    </div>
                    
                    <div class="bestfriends cs-style-3 wow fadeInUp" data-wow-delay="0.5s">
                        <a href="#">
                            <figure>
                                <img src="images/bridesmaids-4.jpg" alt="friends" class="img-responsive">
                            </figure>
                        </a>
                        <h4><a href="#">Christina Lewis</a></h4>
                        <h5>Room Mate</h5>
                    	<div class="social-icons">
                    	<ul>
                            <li class="twitter">
                            <a href="http://www.twitter.com" target="_blank">Twitter</a>
                            </li>
                            
                            <li class="facebook">
                            <a href="http://www.facebook.com" target="_blank">Facebook</a>
                            </li>
                            
                            <li class="youtube">
                            <a href="http://www.youtube.com" target="_blank">YouTube</a>
                            </li>
                            
                            <li class="googleplus">
                            <a href="http://www.plus.google.com" target="_blank">Google +r</a>
                            </li>
                            
                            <li class="pinterest">
                            <a href="http://www.pinterest.com/" target="_blank">Pinterest</a>
                            </li>                                                                                                                                              
                    	</ul>
                    </div>
                    </div>
                </div>

                           
        	</div>
        </div>
        <div class="separator"></div>
    </section>
    
    <section id="the-day">
        <div class="container">
        	<div class="row">
				<div class="col-md-12">
                	<div class="heading">
                        <h2 class="wow fadeInUp underline padtop50" data-wow-delay="0.5s">The Day</h2>
                        <p class="wow fadeInUp" data-wow-delay="0.7s">
						  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam rhoncus, leo at pretium commodo, dui ipsum tristique dolor, in ullamcorper tellus velit quis nisl. In placerat volutpat elit quis dictum.
                        </p>                                             
                    </div>
                    <div class="col-md-6">
                        <div class="venue wow fadeInUp" data-wow-delay="0.7s">
                            <div id="owl-venu" class="owl-carousel">
                                <div class="owl-item">                     
									<img class="img-responsive" src="http://www.webyzona.com/templates/themeforest/ilove/images/venue-1.jpg" alt="venue" />                         
                                </div> 
                                
                               <!--  <div class="owl-item">                    
                                	<img class="img-responsive" src="{{ asset('templates/ilove/images/venue-2.jpg') }}" alt="venue" />                          
                                </div>      -->                                                                                                                
                    		</div>
                            <h4>Program Details</h4>
                            <!-- <p>Check the map below to get location direction</p> -->
                        </div>
                    </div>
                    <div class="col-md-6">
                        
                        <?php $i = 0.7; ?> 
                      @foreach($couple->schedules as $e)
                      <div class="day-details wow fadeInUp" data-wow-delay="{{ $i }}s">
                            <h4>{{ $e->name }}</h4>
                            <p>{{ $e->info }}</p>
                        	<h4><strong>{{ date('d, F Y', strtotime($e->date)) }} <small>{{ $e->time }}</small></strong></h4>
                      </div>
                        <php $i = $i + 0.2 ?>
                      @endforeach
                      <!--   
                      <div class="day-details wow fadeInUp" data-wow-delay="0.9s">
                            <h4>Wedding Program</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam rhoncus, leo at pretium commodo, dui ipsum tristique dolor, in ullamcorper tellus velit quis nisl.</p>
                        	<h4><strong><a href="#">RSVP</a></strong></h4>
                        </div>

                      <div class="day-details wow fadeInUp" data-wow-delay="1s">
                            <h4>Accomodation</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam rhoncus, leo at pretium commodo, dui ipsum tristique dolor, in ullamcorper tellus velit quis nisl.</p>                        	<h4><strong><a href="#">Book Now</a></strong></h4>
                        </div>  -->                                               
                    </div> 
                </div>
                <div class="col-md-12">
                	<div id="map" class="wow fadeInUp" data-wow-delay="0.7s"></div>
                </div>
                <div class="col-md-12">
                	<div class="heading">
                        <h2 class="wow fadeInUp underline padtop50" data-wow-delay="0.5s">Gift List</h2>
                        <p class="wow fadeInUp" data-wow-delay="0.7s">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam rhoncus, leo at pretium commodo, dui ipsum tristique dolor, in ullamcorper tellus velit quis nisl. In placerat volutpat elit quis dictum.
                        </p>                                             
                    </div>                    
                    <div id="owl-gifts" class="owl-carousel wow fadeInUp" data-wow-delay="0.7s">
                        <div class="owl-item">                     
                            <a href="#"><img src="images/gift-1.png" alt="gift" /></a>                        
                        </div>             
                        <div class="owl-item">                    
                            <a href="#"><img src="images/gift-2.png" alt="gift" /></a>                         
                        </div>
                        <div class="owl-item">                    
                            <a href="#"><img src="images/gift-3.png" alt="gift" /></a>                          
                        </div>  
                        <div class="owl-item">                     
                            <a href="#"><img src="images/gift-1.png" alt="gift" /></a>                         
                        </div>             
                        <div class="owl-item">                    
                            <a href="#"><img src="images/gift-2.png" alt="gift" /></a>                         
                        </div>
                        <div class="owl-item">                    
                            <a href="#"><img src="images/gift-3.png" alt="gift" /></a>                          
                        </div>
                        <div class="owl-item">                     
                            <a href="#"><img src="images/gift-1.png" alt="gift" /></a>                         
                        </div>             
                        <div class="owl-item">                    
                            <a href="#"><img src="images/gift-2.png" alt="gift" /></a>                          
                        </div>
                        <div class="owl-item">                    
                            <a href="#"><img src="images/gift-3.png" alt="gift" /></a>                         
                        </div>  
                        <div class="owl-item">                     
                            <a href="#"><img src="images/gift-1.png" alt="gift" /></a>                         
                        </div>             
                        <div class="owl-item"> 
                        	<a href="#"><img src="images/gift-2.png" alt="gift" /></a>
                        </div>
                        <div class="owl-item">                    
                            <a href="#"><img src="images/gift-3.png" alt="gift" /></a>                          
                        </div>                                                                                                                                                           
                    </div>                              
                
                </div>                       
        	</div>
        </div>                
        <div class="separator"></div>
    </section>



        <div class="bigmoments">
                        <h2 class="wow fadeInUp underline padtop50" data-wow-delay="0.5s">Big momments</h2>


            <div style="padding:0px 0;">
                <div id="thumbnail-slider">

                    <div class="inner">
                        <ul>
                        
                           @foreach($galleryPixs as $p)
                            <li> <a class="thumb" href="{{ env('picdisplay').$p }}"></a> </li>
                            @endforeach
                           
                        </ul>
                    </div>
                </div>
            </div>
        </div>



    <section class="white contact" id="contactus">
        <!--Contact Form-->
        <div class="container">
            <div class="row">
            <div class="col-md-12">
                <div class="heading">
                    <h2 data-wow-delay="0.5s" class="wow fadeInUp underline padtop50 animated" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Get in Touch</h2>
                    <p data-wow-delay="0.5s" class="wow fadeInUp animated" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
                </div>

                <div id="ContactFormDiv"></div>

                <form id="contactUS" name="ContactForm" action="{{ route('contact-us') }}" method="post" class="form-horizontal">
                    
                    {!! csrf_field() !!}

                    <input type="hidden" name="couple_id" value="{{ $couple->id }}"> 

                    <div class="col-md-4">
                      <input type="text" placeholder="Your Name" id="name" required name="name" class="form-control">
                    </div>

                    <div class="col-md-4">
                      <input type="text" placeholder="Email" id="email" required name="email" class="form-control">
                    </div>

                    <div class="col-md-4">
                      <input type="text" placeholder="Phone" id="phone" required name="phone" class="form-control">
                    </div>
                  
                    <div class="col-md-12">
                      <textarea rows="10" class="form-control" name="comments"></textarea>
                    </div>
                    
                    <div class="col-md-12 btn-normal">
                        <!-- <a id="submit" href=""><span>Submit</span></a> -->
                       <input type="submit" class="btn" value="Submit">
                    </div>
                </form>
                <div id="flash" style="color:red"></div>

            </div>
        </div>
        </div>
    </section>       

	<!--Footer Section-->
    <section class="footer">    
    	<!--Footer One-->
           
    	<!--Footer Two-->
    	<div class="container-fluid dark footer2">
        	<div class="container">
            	<div class="row">        	
                <div class="col-md-12">
                	<!--<img src="images/logo-grey.png" alt="logo"> -->                  
                    <p>© Webyzona 2016 - All Rights Reserved. Jenorah COmpany</p>
                    <div class="social-icons">
                    	<ul>
                            <li class="twitter">
                            <a href="http://www.twitter.com" target="_blank">Twitter</a>
                            </li>
                            
                            <li class="facebook">
                            <a href="http://www.facebook.com" target="_blank">Facebook</a>
                            </li>
                            
                            <li class="youtube">
                            <a href="http://www.youtube.com" target="_blank">YouTube</a>
                            </li>
                            
                            <li class="googleplus">
                            <a href="http://www.plus.google.com" target="_blank">Google +r</a>
                            </li>
                            
                            <li class="pinterest">
                            <a href="http://www.pinterest.com/" target="_blank">Pinterest</a>
                            </li>   

                            <li class="linkedin">
                            <a href="http://www.linkedin.com" target="_blank">Linkedin</a>
                            </li>                                                                                                                                               
                    	</ul>
                    </div>
                </div>
            </div>
            </div>
    	</div>
    </section>  





    <!-- Al JS Plugins -->
    <script src="{{ asset('templates/ilove/js/modernizr.custom.js')}}"></script>
	<script src="{{ asset('templates/ilove/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAw4zoMhjBovBpFofL610c27FI92dOuEs8"></script>
    <script src="{{ asset('templates/ilove/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('templates/ilove/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('templates/ilove/js/jquery.easing.min.js')}}"></script>
    <script src="{{ asset('templates/ilove/js/jquery.sticky.js')}}"></script>
    <script src="{{ asset('templates/ilove/js/jquery.parallax.js') }}"></script>
    <script src="{{ asset('templates/ilove/js/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('templates/ilove/js/annyang.min.js') }}"></script>
    <script src="{{ asset('templates/ilove/js/wow.min.js') }}"></script>
	<script src="{{ asset('templates/ilove/js/waypoints.min.js') }}"></script>
	<script src="{{ asset('templates/ilove/js/smoothscrolling.js')}}"></script>
    <script src="{{ asset('templates/ilove/js/custom.wedding.js')}}"></script>    
	<script src="{{ asset('templates/ilove/js/fss.js') }}"></script>      
    <script src="{{ asset('templates/ilove/js/geo.js') }}"></script>      
    <script src="{{ asset('templates/ilove/js/jquery.countTo.js') }}"></script>
    <script src="{{ asset('templates/ilove/js/jquery.mixitup.min.js') }}"></script>
    <script src="{{ asset('templates/ilove/js/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('templates/ilove/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('templates/ilove/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('templates/ilove/js/slide-fade-content.js') }}"></script>
    <script src="{{ asset('templates/ilove/js/twitter/jquery.tweet.js') }}"></script>
    <script src="{{ asset('templates/ilove/js/prefixfree.js') }}"></script>
    <script src="{{ asset('templates/ilove/js/countup.js') }}"></script>
    <script src="{{ asset('templates/ilove/js/classie.js') }}"></script>
	<script src="{{ asset('templates/ilove/js/stepsForm.js') }}"></script>
    <script src="{{ asset('templates/ilove/js/modernizr.hover.js') }}"></script>
    <script src="{{ asset('templates/ilovethumbnail-slider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin/jquery.form.js') }}" type="text/javascript"></script>


        <script>

        var marriage_date = "{{ date('Y/m/d', strtotime($couple->marriage_date)) }}"

            setInterval(function() {
                var timespan = countdown(new Date(marriage_date), new Date());
                var div = document.getElementById('time');
                div.innerHTML = "</div>" + "<div><span>Months</span>" + timespan.months + "</div>" + "<div><span>Days</span>" + timespan.days + "</div>" + "<div><span>Hours</span>" + timespan.hours + "</div>" + "<div><span>Minutes</span>" + timespan.minutes + "</div>" + "<div><span>Seconds</span>" + timespan.seconds + "</div>"
            }, 1000);


            $('#contactUS').ajaxForm({ 
                    headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
                    beforeSubmit:btn,
                    success:showResponse
            }); 

                    function btn(){
                        console.log('Logging things')
                    }

                    function showResponse(){
                        console.log('Am here for you.')
                        $('#flash').html('Your comments was successfully submitted')
                        

                        setTimeout(function(){
                            $('#flash').hide()
                        }, 3000);

                        $("#contactUS")[0].reset()

                    }

    </script>


  </body>
</html>

