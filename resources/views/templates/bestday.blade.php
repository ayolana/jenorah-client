
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ ucwords($couple->slug) }} | Jenorah Client Website</title>

<!-- Bootstrap CSS -->
<link href="{{ asset('templates/bestday/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{ asset('templates/bestday/css/bootstrap-theme.css')}}" rel="stylesheet">
<!-- Font Awesome CSS -->
<link href="{{ asset('templates/bestday/css/font-awesome.min.css')}}" rel="stylesheet">
<!-- Flaticon CSS -->
<link href="{{ asset('templates/bestday/css/flaticon/flaticon.css')}}" rel="stylesheet">
<!-- Owl Carousel CSS -->
<link href="{{ asset('templates/bestday/css/owl.carousel.css')}}" rel="stylesheet">
<link href="{{ asset('templates/bestday/css/owl.transitions.css')}}" rel="stylesheet">
<!-- ColorBox CSS -->
<link href="{{ asset('templates/bestday/css/colorbox.css')}}" rel="stylesheet">
<!-- Theme CSS -->
<link href="{{ asset('templates/bestday/css/style.css')}}" rel="stylesheet">

<!-- Color CSS -->
<link href="{{ asset('templates/bestday/css/brown.css')}}" rel="stylesheet">



<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>

    <script src="{{ asset('templates/bestday/js/jquery-1.11.0.min.js')}}"></script> 

</head>

<body id="skrollr-body">


 	<!-- Header -->
    <header>
        <div class="header_plane anim"></div>
        
        <!-- Menu Button -->
        <a class="main_menu_btn">
            <span class="line line1"></span>
            <span class="line line2"></span>
            <span class="line line3"></span>
        </a>
        
        
        <!-- Submenu -->
        <div class="main_menu_block">
            <div class="menu_wrapper">
                <div class="sub_menu anim">
                    <ul>
                        <li><a href="#our_story">Our Story</a></li>
                        <li><a href="#guests">Our Bridal Party</a></li>
                        <li><a href="#when_where">When & Where</a></li>
                        <li><a href="#hotel">Hotel Reservation</a></li>
                        <li><a href="#events">Our Events</a></li>
                        <li><a href="#rsvp">RSVP</a></li> 
                        <li><a href="#gallery">Our Gallery</a></li> 
                        <li><a href="#registry">Gift Registry</a></li>                    
                    </ul>
                </div>
                <div class="sub_img anim"></div>
            </div>
        </div>
        <!-- Submenu End -->
        
		<!-- Social Buttons -->
        <div class="header_social">
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-pinterest"></i></a>
        </div>	
        
    </header>
    <!-- Header End -->

<div class="page">
 
    <!-- Intro -->
    <section class="home_intro white_txt parallax2" data-image="{{ asset('templates/bestday/images/back.jpg')}}">        
    	<div class="home_txt" data-0="opacity:1" data-top-bottom="opacity:0">
            
            <!-- Intro Text -->
            
            @if(time() < strtotime($couple->marriage_date))
                <div class="title1 title1_2">Getting Married</div>
                <div class="bullet"><span>{{ date('d', strtotime($couple->marriage_date)) }} <b>{{ date('m', strtotime($couple->marriage_date)) }}</b></span></div>
                <div class="title0">{{ date('Y', strtotime($couple->marriage_date)) }}</div>
            @else
                <div class="title1 title1_2">Got Married on</div>
                <div class="bullet"><span>{{ date('d', strtotime($couple->marriage_date)) }} <b>{{ date('m', strtotime($couple->marriage_date)) }}</b></span></div>
                <div class="title0">{{ date('Y', strtotime($couple->marriage_date)) }}</div>
            @endif

            <a href="#married" class="intro_down"><span><i class="fa fa-angle-down"></i></span></a>
        </div>
    	<div class="into_firefly"></div>   
    </section>
	<!-- Intro End -->
    
    <!-- Married -->
    <section class="married clearfix" id="married">
        <div class="container">
                <div class="im1 parallax"  data-bottom="left:20%" data-center="left:0" data-image="{{ asset('templates/bestday/images/im1.jpg')}}"><div class="im_arrows"></div></div>
                <div class="im2 parallax" data-bottom="right:20%" data-center="right:0" data-image="{{ asset('templates/bestday/images/im2.jpg')}}"><div class="im_arrows"></div></div>

                <div class="married_txt" data-bottom="opacity:0" data-center="opacity:1">
                    @if(time() < strtotime($couple->marriage_date))
                        <h2><span>You're</span>   Invited</h2>
                    On the 
                    @endif
                    {{ date('dS F Y', strtotime($couple->marriage_date)) }}<br>

                    <b>{{ $couple->bride->full_name }} & {{ $couple->groom->full_name }}</b><br>
                    eloped in {{ $couple->wedding_location }}.<br>
                    <a href="#rsvp" class="btn go">RSRP NOW</a> 
                    
                    <div class="married_coundown"></div>
                    <div class="double_arrow"></div>
                </div>
        </div>
    </section>
    <!-- Married End -->

    
    <!-- Story -->
    <section class="our_story" id="our_story">
            <h2><span>Our</span>  Story</h2>
            
            <!-- Wrapper -->
            <div class="story_wrapper">
                
                <!-- Item -->
                <div class="story_item">
                    <div class="story_img parallax" data-image="{{ asset('templates/bestday/images/port2.jpg')}}"><div class="story_img_plane"></div></div>
                    <div class="story_plane">
                        <div class="story_back"></div>
                        <div class="story_txt">
                            <div class="story_title">
                                The day we met
                                <i>03.12.2013</i>
                            </div>
                             Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                             tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                             quis nostrud exercitation ullamco.
                        </div> 
                    </div>
                </div>

                <!-- Item -->
                <div class="story_item">
                    <div class="story_img parallax" data-image="{{ asset('templates/bestday/images/port3.jpg')}}">
                        <a class="youtube cboxElement" href="http://www.youtube.com/embed/hEowkXOP_Wg?rel=0">         
                        <i class="flaticon-play102"></i></a>
                    <div class="story_img_plane"></div></div>
                    <div class="story_plane">
                        <div class="story_back"></div>
                        <div class="story_txt">
                              <div class="story_title">
                                Our first holiday
                                <i>03.01.2014</i>
                            </div>
                             Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                             tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                             quis nostrud exercitation ullamco.
                        </div> 
                    </div>
                </div>

                <!-- Item -->
                <div class="story_item">
                    <div class="story_img parallax" data-image="{{ asset('templates/bestday/images/port1.jpg')}}"><div class="story_img_plane"></div></div>
                    <div class="story_plane">
                        <div class="story_back"></div>
                        <div class="story_txt">
                              <div class="story_title">
                                First dinner together
                                <i>12.05.2014</i>
                            </div>
                             Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                             tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                             quis nostrud exercitation ullamco.
                        </div> 
                    </div>
                </div>

                <!-- Item -->
                <div class="story_item">
                    <div class="story_img parallax" data-image="{{ asset('templates/bestday/images/port4.jpg')}}"><div class="story_img_plane"></div></div>
                    <div class="story_plane">
                        <div class="story_back"></div>
                        <div class="story_txt">
                              <div class="story_title">
                                The Proposal
                                <i>24.08.2014</i>
                            </div>
                             Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                             tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                             quis nostrud exercitation ullamco.
                        </div> 
                    </div>
                </div>

                <!-- Item -->
                <div class="story_item">
                    <div class="story_img parallax" data-image="{{ asset('templates/bestday/images/port5.jpg')}}"><div class="story_img_plane"></div></div>
                    <div class="story_plane">
                        <div class="story_back"></div>
                        <div class="story_txt">
                              <div class="story_title">
                                Here comes the baby
                                <i>10.01.2015</i>
                            </div>
                             Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                             tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                             quis nostrud exercitation ullamco.
                        </div> 
                    </div>
                </div>

                <!-- Item -->
                <div class="story_item">
                    <div class="story_img parallax" data-image="images/port6.jpg"><div class="story_img_plane"></div></div>
                    <div class="story_plane">
                        <div class="story_back"></div>
                        <div class="story_txt">
                              <div class="story_title">
                                Bridesmaids
                                <i>03.02.2015</i>
                            </div>
                             Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                             tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                             quis nostrud exercitation ullamco.
                        </div> 
                    </div>
                </div>
                 
        </div>
    </section>

   

    <!-- When & Where -->
    <section class="when_where white_txt parallax" id="when_where" data-image="images/back5.jpg" data-bottom-top="opacity:0;" data-bottom="opacity:1;">
        <div class="over"></div>
        <div class="container">
               
            <!-- Photocamera Icon -->
            <div class="photocamera"><span class="flaticon-slr2"></span></div>

            <div class="when_where_container opacity">
            <h2><span>Where &amp;</span> When</h2>
                
                <!-- Texts -->
                <div class="pattern1"></div>
                <div class="title1">The Historic</div>
                <div class="title2">Roundhouse Depot</div>
                <div class="title3"><a href="https://goo.gl/maps/dhx42" target="_blank">
                320 Church Street <br />Huntsville, Alabama</a><br /><i class="flaticon-map35"></i> </div>

                <div class="pattern2"></div>
                <div class="title1">5 o'clock</div>
                <div class="title5">IN THE EVENING</div>
                <div class="title4">September Nineteen<br>Two Thousand and Fifteen</div>
                <div class="pattern3"></div>
            </div>
        </div>
    </section>
    <!-- When & Where End -->


   
    <!-- Events -->
    <section class="our_story events" id="events">
            <h2><span>Our</span>  Events</h2>
            
            <!-- Wrapper -->
            <div class="story_wrapper">
                
                <!-- Item -->
                @foreach($couple->schedules as $e)
                <div class="story_item">
                    <div class="story_img parallax" data-image="images/port7.jpg"><div class="story_img_plane"></div></div>
                    <div class="story_plane">
                        <div class="story_back"></div>
                        <div class="story_txt">
                            <div class="story_title">
                                {{ $e->name }}
                                <i>{{ date('d, F Y', strtotime($e->date)) }} <small>{{ $e->time }}</small></i>
                            </div>
                            {{ $e->info }}
                        </div> 
                    </div>
                </div>
                @endforeach
                
              
                
            
        </div>
    </section>
    <!-- Events End -->


    <!-- Gallery -->
    <section class="gallery" id="gallery">
        <h2><span>Our</span> Gallery</h2>    
        <div class="gallery_wrapper">
           

            @foreach($galleryPixs as $p)
            <div class="gallery_item">
                <div class="gallery_txt">
                   <a href="images/port1.jpg" title="In late August, on a family vacation to Oahu"><b>Hawaii</b>
                   In late August, on a family vacation to Oahu
                   </a>
                </div>
                <img src="{{ env('picdisplay').$p }}" alt="Gallery 1">    
            </div>
            @endforeach
           
        </div>
    </section>
    <!-- Gallery End -->



    <!-- Gift Registry -->
    <section class="registry hidden" id="registry">
        <div class="container">
            <h2><span>Gift</span> Registry</h2>
            
            <p>What we want most for our wedding is to have our friends and family there to celebrate the occasion with us. <br>
            So more than anything we’re simply grateful for your presence!</p>
            <p>If you would like to get us something, we’d love that too...you can find our registries here:</p>
            
            <!-- Logos & Links -->
            <div class="registry_wrapper">
                <a href="#"><img src="images/amazon.png" alt="Amazon"></a>
                <a href="#"><img src="images/johnlewis.png" alt="JohnLewis"></a>
                <a href="#"><img src="images/belk.png" alt="Belk"></a>
                <a href="#"><img src="images/bedbathbeyond.png" alt="BedBathBeyond"></a>
            </div>
        </div>
    </section>
    <!-- Gift Registry End -->
    
    <!-- RSVP -->
    <section class="rsvp" id="rsvp">
        <div class="container">
            <h2><span>Join</span> Our Party</h2>

            <div id="envelope" data-100-top="@class:active" data-200-bottom="@class: ">
                <div class="envelope_front"><div class="env_top_top"></div></div>
                <div class="envelope_back"><div class="env_top"></div></div>

                <div class="paper">
       
                    <!-- End Date of Reservation -->
                    <div class="paper_title">Please RSVP by August 2nd</div>
                    
                    <!-- Form -->
                <form id="contactUS" name="ContactForm" action="{{ route('contact-us') }}" method="post" class="form-horizontal">
                    
                     {!! csrf_field() !!}

                    <input type="hidden" name="couple_id" value="{{ $couple->id }}"> 

                    <div id="div_block_1">
                        <div class="txt_input">
                            <input type="text" class="form-control" name="name"  placeholder="Your Name">
                        </div>
                        <div class="txt_input">
                            <input type="text" class="form-control" name="phone"  placeholder="Your phone no">
                         </div>
                        <div class="txt_input">
                            <input type="text" class="form-control" name="email"  placeholder="Your E-mail">
                        </div>
                        <div class="txt_input">
                            <input type="text" class="form-control" name="guests"  placeholder="# Attending">
                        </div>
                        <input name="" type="submit" value="Send" class="btn btn-lg submit_block_1">
                       
                       </div>
                        <!-- Form Additional text -->
                        <p>We’re excited to see you! Any questions, just email us at: 
                        <a href="mailto:{{$couple->user->email}}">email us</a></p>
                    </div>


                </div>
                <div class="container">

             <!-- Thanks Text -->
            <div class="thanks">Thanks for Visiting our Website</div>
            <div class="footer_txt">

               


                <!-- Social Buttons -->
                <div class="footer_social">
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-pinterest"></i></a>
                </div>
                
                            <div class="title1">{{ $couple->bride->display)_name }} & {{ $couple->groom->display_name }}</div>
                <!-- Copyrights -->
                <div class="copyrights">2016 Jenorah Company</div>
                
            </div>
        </div>
                
            </div>
        </div>

    </section> 
    <!-- RSVP End --> 

   
    
    <!-- JQuery -->
    <!-- CountDown JS -->
    <script type="text/javascript" src="{{ asset('templates/bestday/js/jquery.plugin.min.js')}}"></script> 
	<script type="text/javascript" src="{{ asset('templates/bestday/js/jquery.countdown.min.js')}}"></script>

 	<!-- ColorBox JS -->
    <script src="{{ asset('templates/bestday/js/jquery.colorbox-min.js')}}"></script>
    <!-- OWL Carousel JS -->
	<script src="{{ asset('templates/bestday/js/owl.carousel.min.js')}}"></script>
    <!-- Bootstrap JS -->
	<script src="{{ asset('templates/bestday/js/bootstrap.min.js')}}"></script>

    <!-- ScrollR JS -->
    <script src="{{ asset('templates/bestday/js/skrollr.min.js')}}"></script>
    

    
    <!-- PrefixFree -->
    <script src="{{ asset('templates/bestday/js/prefixfree.min.js')}}"></script>
    
    <!-- FireFly JS -->
    <script src="{{ asset('templates/bestday/js/jquery.firefly-0.3-min.js')}}"></script>
    <!-- Theme JS -->
    <script src="{{ asset('templates/bestday/js/script.js')}}"></script>
     <script src="{{ asset('admin/jquery.form.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin/notify.min.js') }}" type="text/javascript"></script>
    
    <script>
        var marriage_date = "{{ date('Y, m, d', strtotime($couple->marriage_date)) }}"

    $('.married_coundown').countdown({until: new Date(marriage_date)});

    $('#contactUS').ajaxForm({ 
                    headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
                    beforeSubmit:btn,
                    success:showResponse
            }); 

                    function btn(){
                        console.log('Logging things')
                    }

                    function showResponse(){
                        console.log('Am here for you.')
                        $('#flash').html('Your comments was successfully submitted')
                        

                        setTimeout(function(){
                            $('#flash').hide()
                        }, 3000);

                        $("#contactUS")[0].reset()

                    }
    </script>
</div>

</body>
</html>
