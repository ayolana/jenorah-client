
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <title>{{ ucwords($couple->slug) }} | Jenorah Client Website</title>
    <meta charset="utf-8">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" type="text/css" href="{{ asset('templates/hotboot/css/jquery.countdown.css') }}"> 
    <link rel="stylesheet" href="{{ asset('templates/hotboot/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/hotboot/css/main.css') }}" id="color-switcher-link">
    <link rel="stylesheet" href="{{ asset('templates/hotboot/css/animations.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/hotboot/css/fonts.css') }}">
    <script src="{{ asset('templates/hotboot/js/vendor/modernizr-2.6.2.min.js') }}"></script>

    <!--[if lt IE 9]>
        <script src="js/vendor/html5shiv.min.js"></script>
        <script src="js/vendor/respond.min.js"></script>
    <![endif]-->

</head>
<body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

<div id="box_wrapper">
  
    <section id="mainslider">

                        <div class="flexslider">
                            <ul class="slides">
                                <li>
                                    <div class="slide_overlay">
                                      <img src="{{ asset('templates/hotboot/example/flex_slide01.jpg')}}" alt="">                      
                                    </div>
                                    
                                </li>
                                <li>
                                    <div class="slide_overlay">
                                      <img src="{{ asset('templates/hotboot/example/flex_slide02.jpg')}}" alt="">                      
                                    </div>
                                   
                                </li>                            
                            </ul>
                        </div>



    </section>

    <section id="count_down">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                   <h2>{{ $couple->bride->display_name }} and {{ $couple->groom->display_name }}</h2>
                </div>
            </div>
           
            @if(time() < strtotime($couple->marriage_date))
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div id="countdown"></div>
                </div>
            </div>
            @endif
            
            @if(time() < strtotime($couple->marriage_date))
            <div class="row">
                <div class="col-sm-12 text-center">
                   <h2>We inviting you on {{ date('l, dS F Y', strtotime($couple->marriage_date)) }}</h2>
                </div>
            </div>
            @else
             <div class="row">
                <div class="col-sm-12 text-center">
                   <h2>Got Married on {{ date('l, dS F Y', strtotime($couple->marriage_date)) }}</h2>
                </div>
            </div>
            @endif
        </div>
    </section>

    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <a href="./" class="navbar-brand"><span>Marryme</span></a>
                    <span id="toggle_mobile_menu"></span>
                    <nav id="mainmenu_wrapper">
                        <ul id="mainmenu" class="nav sf-menu">
                            <li class="">
                                <a href="#top">Home</a>
                            </li>
                            <li class="">
                                <a href="#about_us">About Us</a>
                            </li> 
                           <!--  <li class="">
                                <a href="#family">Family</a>
                             </li>         
                             <li class="">
                                <a href="#invitation">Invitation</a>
                             </li>      
                              <li class="">
                                <a href="#plase">Our Place</a>
                             </li>     -->    
                            <li class="">
                               <a href="#plase">Gallery</a>
                            </li>   
                            <!-- <li class="">
                                <a href="#blog">Blog</a>
                            </li> -->
                            <li class="">
                               <a href="#contact">RSVP</a>
                            </li>    
                            <!-- <li>
                                <a href="icons.html">Features</a>
                                <ul>
                                    <li>
                                        <a href="icons.html">Icons</a>
                                    </li>
                                    <li>
                                        <a href="animation.html">Animation</a>
                                    </li>
                                    <li>
                                        <a href="shortcodes.html">Shortcodes&amp;Widgets</a>
                                    </li>
                                </ul>
                            </li>     -->   
                        </ul>  
                    </nav>
                
                </div>
            </div>
        </div>
    </header>

   

<section id="about_us" class="light_section">
    <div class="container">
        <div class="row to_animate" data-animation="fadeInUp">
            <div class="col-sm-12 text-center">
                <h2 class="section_header">
                   Our Love Story
                </h2>
                <p>{{ $couple->our_love_story }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="teaser with_image to_animate" data-animation="fadeInLeft">
                    <div class="teaser_image img-circle">
                        <img class="img-circle" src="{{ env('picdisplay').$couple->bride->picture }}" alt="">
                    </div>
                    <div class="teaser_description">
                        <h3 class="greatvibes">{{ $couple->bride->full_name }}</h3>
                        <p>{{ $couple->bride->about }}</p>
                       
                    </div>
                </div>
            </div> 
            <div class="col-sm-6">
                <div class="teaser with_image to_animate" data-animation="fadeInRight">
                    <div class="teaser_image">
                        <img class="img-circle" src="{{ env('picdisplay').$couple->groom->picture }}" alt="">
                    </div>
                    <div class="teaser_description">
                        <h3 class="greatvibes">{{ $couple->groom->full_name }}</h3>
                        <p>{{ $couple->groom->about }}</p>
                      
                    </div>
                </div>
            </div> 

        </div>
    </div>
</section>







<section id="plase">
    <div class="container">
         <div class="row to_animate" data-animation="fadeInUp">
            <div class="col-sm-12 text-center">
                <h2 class="section_header">
                   Place Close to Our Heart
                </h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque expedita suscipit odio quas velit eaque consequuntur natus, amet qui.</p>
            </div>
        </div>
    </div> 
    <div class="container-fluid">
    <div class="row to_animate" data-animation="slideUp">
        <div class="col-md-12">
            <div id="related-gallery-items-carousel" class="owl-carousel">

                @foreach($galleryPixs as $p)
                <div class="gallery-item development">
                    <div class="gallery-image">
                        <img src="{{ env('picdisplay').$p }}" alt="">
                        <div class="gallery-image-links">
                            <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="{{ env('picdisplay').$p }}"></a>
                            <!-- <a class="p-link" title="" href="#"></a> -->
                        </div>
                    </div>
                    <div class="gallery-item-description">
                        <h3><a href="#">About {{ $couple->bride->display_name }} & {{ $couple->groom->display_name }} </a></h3>
                        <p> {{ substr($couple->our_love_story, 0, 200) }} </p>
                    </div>
                </div>
                @endforeach

              

             </div>
                
            </div>
        </div>
    </div>
</section>

<section id="gift" class="hidden">
     <div class="container">
        <div class="row to_animate" data-animation="fadeInUp">
            <div class="col-sm-12 text-center">
                <h2 class="section_header">
                   Gift Registry
                </h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque expedita suscipit odio quas velit eaque consequuntur natus, amet qui.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div id="partners-carousel" class="owl-carousel">
                    <div>
                        <a href="#">
                            <img src="example/partner1.png" alt="">
                        </a>
                    </div>
                    <div>
                        <a href="#">
                            <img src="example/partner2.png" alt="">
                        </a>

                    </div>
                    <div>
                        <a href="#">
                            <img src="example/partner3.png" alt="">
                        </a>

                    </div>
                    <div>
                        <a href="#">
                            <img src="example/partner4.png" alt="">
                        </a>

                    </div>
                    <div>
                        <a href="#">
                            <img src="example/partner5.png" alt="">
                        </a>

                    </div>
                    <div>
                        <a href="#">
                            <img src="example/partner6.png" alt="">
                        </a>

                    </div>
                    <div>
                        <a href="#">
                            <img src="example/partner1.png" alt="">
                        </a>
                    </div>
                </div>
            </div>            
        </div> 
    </div>
</section>

<section id="mailus" class="hidden dark_section parallax">
     <div class="container">
        <div class="row to_animate" data-animation="fadeInUp">
            <div class="col-sm-12 text-center">
                <h2 class="section_header">
                   Subscribe For News From Us
                </h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque expedita suscipit odio quas velit eaque consequuntur natus, amet qui.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="widget widget_mailchimp">                
                    <form id="signup" action="/" method="get" class="form-inline">
                        <div class="form-group">
                            <input name="email" id="mailchimp_email" type="email" class="form-control" placeholder="your@email.com">
                        </div>
                        <button type="submit" class="theme_button">Send</button>
                        <div id="response"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="contact" class="light_section">
    <div class="container">
        <div class="row to_animate" data-animation="fadeInUp">
            <div class="col-sm-12 text-center">
                <h2 class="section_header">
                   RSVP
                </h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque expedita suscipit odio quas velit eaque consequuntur natus, amet qui.</p>
            </div>
        </div> 
        <div class="row"> 
            <div class="widget col-sm-6 to_animate">
                <h3>Contact form</h3>
                <form class="contact-form" method="post" id="contactUS" name="ContactForm" action="{{ route('contact-us') }}">
                    <p class="contact-form-name">
                        <label for="name">Name <span class="required">*</span></label>
                        <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Name">
                    </p>


                     {!! csrf_field() !!}

                    <input type="hidden" name="couple_id" value="{{ $couple->id }}"> 

                    <p class="contact-form-email">
                        <label for="email">Email <span class="required">*</span></label>
                        <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email">
                    </p>
                    <p class="contact-form-subject">
                        <label for="subject">Phone <span class="required">*</span></label>
                        <input type="text" aria-required="true" size="30" value="" name="phone" id="subject" class="form-control" placeholder="phone">
                    </p>

                     <p class="contact-form-subject">
                        <label for="subject">Guests <span class="required">*</span></label>
                        <input type="text" aria-required="true" size="30" value="" name="guests" id="subject" class="form-control" placeholder="guests">
                    </p>
                  
                    <p class="contact-form-submit vertical-margin-40">
                        <!-- <input type="submit" value="Send" id="contact_form_submit" name="contact_submit" class="theme_button"> -->
                        <button type="submit" id="contact_form_submit" name="contact_submit" class="type-15">
                         <span>Send!</span>
                         <span></span>
                     </button>
                    </p>
                </form>
            </div>
            <div class="widget widget_contact col-sm-6">
                 <h3>Contact Info</h3>
                 <div id="map"></div>       
                   <div class="widget widget_contact">                
                    <p>Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. In ac felis quis tortor malesuada pretium. In hac habitasse platea dictumst curabitur nisi hac habitasse platea dictumst maecenas nec odio et ante tincidunt tempus morbi ac felis.
                    <br><br>
                        <i class="rt-icon-device-phone"></i><strong>Phone:</strong> +12 345 678 9123
                    </p>
                    <p>
                        <i class="rt-icon-pencil"></i><strong>Email:</strong> <a href="mailto:info@company.com">info@company.com</a>
                    </p>
                    <p>
                        <i class="rt-icon-globe-outline"></i><strong>Website: </strong><a href="./">www.company.com</a>
                    </p>
                    <p>
                        <i class="rt-icon-location-arrow-outline"></i><strong>Address:</strong> 4321 Your Address, Country 
                    </p>

                </div>
            </div>
        </div>
    </div>
</section>



    <section id="copyright" class="dark_section with_top_border">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <p>&copy; Copyright 2014 - Wedding Template</p>
                </div>                
            </div>
        </div>
    </section>
 

</div><!-- eof #box_wrapper -->

<div class="preloader">
    <div class="preloader_image"></div>
</div>


        <!-- libraries -->
        <script src="{{ asset('templates/hotboot/js/vendor/jquery-1.11.1.min.js')}}"></script>
        <script src="{{ asset('templates/hotboot/js/vendor/bootstrap.min.js')}}"></script>
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.appear.js')}}"></script>

        <!-- superfish menu  -->
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.hoverIntent.js')}}"></script>
        <script src="{{ asset('templates/hotboot/js/vendor/superfish.js')}}"></script>
        
        <!-- page scrolling -->
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.easing.1.3.js') }}"></script>
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.nicescroll.min.js') }}"></script>
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.ui.totop.js')}}"></script>
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.localscroll-min.js')}}"></script>
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.scrollTo-min.js')}}"></script>
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.parallax-1.1.3.js') }}"></script>

        <!-- widgets -->
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.easypiechart.min.js')}}"></script><!-- pie charts -->
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.countTo.js')}}"></script><!-- digits counting -->
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.prettyPhoto.js')}}"></script><!-- lightbox photos -->
        <script src="{{ asset('templates/hotboot/js/vendor/jflickrfeed.min.js')}} "></script><!-- flickr -->
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.plugin.min.js')}}"></script><!-- plugin creator for comingsoon counter -->
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.countdown.js') }}"></script><!-- coming soon counter -->
        <script src="{{ asset('templates/hotboot/twitter/jquery.tweet.min.js') }}"></script><!-- twitter -->

        <!-- sliders, filters, carousels -->
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.isotope.min.js')}}"></script>
        <script src="{{ asset('templates/hotboot/js/vendor/owl.carousel.min.js')}}"></script>
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.fractionslider.min.js')}}"></script>
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.flexslider-min.js')}}"></script>
        <script src="{{ asset('templates/hotboot/js/vendor/jquery.bxslider.min.js')}}"></script>

        <!-- custom scripts -->
        <script src="{{ asset('templates/hotboot/js/plugins.js')}}"></script>
        <script src="{{ asset('templates/hotboot/js/main.js')}}"></script>

 <script src="{{ asset('admin/jquery.form.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin/notify.min.js') }}" type="text/javascript"></script>
        <!-- Map Scripts -->

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript">

        // var marriage_date = "{{ date('Y, m, d', strtotime($couple->marriage_date)) }}"

     //    //comingsoon counter
     // if (jQuery().countdown) {
     //  jQuery('#countdown').countdown({until: new Date(2015, 5-1, 15)});
     //  // jQuery('#countdown').countdown('pause')
     // };

       var marriage_date = "{{ date('Y, m, d', strtotime($couple->marriage_date)) }}"

    $('#countdown').countdown({until: new Date(marriage_date)});


            var lat;
            var lng;
            var map;
            var styles = [{"featureType":"water","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]},{"featureType":"landscape","stylers":[{"color":"#f2e5d4"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},{"featureType":"administrative","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"road"},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{},{"featureType":"road","stylers":[{"lightness":20}]}];

            //type your address after "address="
            jQuery.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address=london, baker street, 221b&sensor=false', function(data) {
                lat = data.results[0].geometry.location.lat;
                lng = data.results[0].geometry.location.lng;
            }).complete(function(){
                dxmapLoadMap();
            });

            function attachSecretMessage(marker, message)
            {
                var infowindow = new google.maps.InfoWindow(
                    { content: message
                    });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map,marker);
                });
            }

            window.dxmapLoadMap = function()
            {
                var center = new google.maps.LatLng(lat, lng);
                var settings = {
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoom: 16,
                    draggable: false,
                    scrollwheel: false,
                    center: center,
                    styles: styles 
                };
                map = new google.maps.Map(document.getElementById('map'), settings);

                var marker = new google.maps.Marker({
                    position: center,
                    title: 'Map title',
                    map: map
                });
                marker.setTitle('Map title'.toString());
            //type your map title and description here
            attachSecretMessage(marker, '<h3>Map title</h3>Map HTML description');
            }


            $('#contactUS').ajaxForm({ 
                    headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
                    beforeSubmit:btn,
                    success:showResponse
            }); 

                    function btn(){
                        console.log('Logging things')
                    }

                    function showResponse(){
                        console.log('Am here for you.')
                        $('#flash').html('Your comments was successfully submitted')
                        

                        setTimeout(function(){
                            $('#flash').hide()
                        }, 3000);

                        $("#contactUS")[0].reset()

        }


        </script>


    </body>
</html>