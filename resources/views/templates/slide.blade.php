
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="description" content="Clean Slide Responsive Vcard Template" />
<meta name="keywords" content="jquery, Responsive Vcard, Template, Vcard, Clean Slide" />

    <title>{{ ucwords($couple->slug) }} | Jenorah Client Website</title>

<!-- Loading Google Web fonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Rouge+Script' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>

<!-- CSS Files -->
<link href="{{ asset('templates/slide/css/reset.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('templates/slide/css/style.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('templates/slide/css/colour_21.css') }}" rel="stylesheet" type="text/css"  id="color" />
<link href="{{ asset('templates/slide/css/typography.css') }}" rel="stylesheet" type="text/css"  id="customFont"/>
<link href="{{ asset('templates/slide/css/responsive.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ asset('templates/slide/css/prettyPhoto.css') }}" type="text/css" />

<!-- settings-panel Demo Purpose css -->
<link href="{{ asset('templates/slide/settings/settings.css')}}" rel="stylesheet" type="text/css" />

<link rel="shortcut icon" href="{{ asset('templates/slide/images/favicon.ico') }}">
<link rel="apple-touch-icon" href="i{{ asset('templates/slide/mages/apple_touch_icon.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('templates/slide/images/apple_touch_icon_72x72.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('templates/slide/images/apple_touch_icon_114x114.png')}}">

</head>

<body>

<!--Preloader- -->
<div id="preloader">
  <div id="spinner_container">
    <img id="spinner" src="{{ asset('templates/slide/images/loading.gif') }}" alt="" />
  </div>
</div>
<!-- //Preloader -->

<!----------------------------------------------------------
	/*settings-panel  START	 */
----------------------------------------------------------->
	<div class="settings-panel">
		<span class="open-buttton closed" title="background pattern"><br/><br/></span>
				<h5>Background Pattern</h5>
		<div  class="bg_patterns">
			<ul class="patterns">
				<li></li>
				<li class="bgs24" title="soft_wallpaper"></li>
				<li class="bgs1" title="45degreee_fabric"></li>
				<li class="bgs14" title="xv"></li>
				<li class="bgs4" title="bright_squares"></li>
				<li class="bgs5" title="hexellence"></li><li></li>
				<li class="bgs31" title="grunge_wall"></li>
				<li class="bgs7" title="rockywall"></li>
				<li class="bgs8" title="lghtmesh"></li>
				<li class="bgs35" title="tex2res3"></li>
				<li class="bgs10" title="light_wool"></li><li></li>
				<li class="bgs11" title="diagonal-noise"></li>
				<li class="bgs12" title="fabric_plaid"></li>
				<li class="bgs25" title="square_bg"></li>
				<li class="bgs2" title="bgnoise_lg"></li>
				<li class="bgs15" title="light_alu"></li><li></li>
				<li class="bgs28" title="subtle_stripes"></li>
				<li class="bgs18" title="project_papper"></li>
				<li class="bgs20" title="ravenna"></li>
				<li class="bgs21" title="roughcloth"></li>
				<li class="bgs22" title="scribble_light"></li><li></li>
				<li class="bgs26" title="subtle_freckles"></li>
				<li class="bgs29" title="tiny_grid"></li>
				<li class="bgs30" title="smooth_wall"></li>
				<li class="bgs32" title="concrete_wall"></li>
                <li class="bgs32" title="Reset" >Reset</li>
			</ul>
		</div>
					<div  class="colour_patterns">
			<h5>Pre Defined Color</h5>
			<ul class="c_patterns"><li> </li>
				<li  class="c1" title="Tonys Pink" ></li>
				<li  class="c2" title="Eggplant" ></li>
				<li  class="c3" title="Strikemaster" ></li>
				<li  class="c4" title="Cupid"></li>
				<li  class="c5" title="Chelsea Cucumber"></li><li> </li>
				<li  class="c6" title="De York"></li>
				<li  class="c7" title="Deep Blush" ></li>
				<li  class="c8" title="Celeste" ></li>
				<li  class="c9" title="Salem" ></li>
				<li  class="c10" title="Yellow Green" ></li><li> </li>
				<li  class="c12" title="Trendy Pink" ></li>
				<li  class="c13" title="Apricot" ></li>
				<li  class="c14" title="Kabul" ></li>
				<li  class="c15" title="Bouquet" ></li>
				<li  class="c16" title="Brandy Rose" ></li>
				<li  class="c17" title="Highland" ></li>
				<li  class="c18" title="Teal" ></li>
				<li  class="c19" title="Rodeo Dust" ></li>
				
				<li  class="c1" title="Reset">Reset</li>
			</ul>
		</div>
		
		<div  class="card_patterns">
			<h5>Card  Patterns</h5>
			<ul class="patterns">
				<li></li>
				<li class="card_bgs1" title="45degreee_fabric"></li>
				<li class="card_bgs2" title="bgnoise_lg"></li>
				<li class="card_bgs4" title="bright_squares"></li>
				<li class="card_bgs36" title="wall4"></li><li class="card_bgs14" title="xv"></li>
				<li></li>
				<li class="card_bgs6" title="cork"></li>
				<li class="card_bgs7" title="rockywall"></li>
				<li class="card_bgs8" title="lghtmesh"></li>
				<li class="card_bgs10" title="light_wool"></li>
				<li class="card_bgs26" title="subtle_freckles"></li><li></li>
				<li class="card_bgs12" title="fabric_plaid"></li>
				<li class="card_bgs5" title="hexellence"></li>
				<li class="card_bgs15" title="light_alu"></li> 
				<li class="card_bgs18" title="project_papper"></li>
				<li class="card_bgs20" title="ravenna"></li><li></li>
				<li class="card_bgs21" title="roughcloth"></li>
				<li class="card_bgs24" title="soft_wallpaper"></li>
				<li class="card_bgs11" title="diagonal-noise"></li>
				<li class="card_bgs29" title="tiny_grid"></li>
				<li class="card_bgs30" title="textured_stripes"></li><li></li>
				<li class="card_bgs31" title="extra_clean_paper"></li>
				<li class="card_bgs32" title="tex2res2"></li>
				<li class="card_bgs33" title="egg_shell"></li>
				<li class="card_bgs39" title="frenchstucco"></li>
				<li class="card_bgs36" title="Reset"></li>
			</ul>
		</div>
		


</div>
<!-- settings-panel  END-->


<!-- settings-panel -->
	<div class="settings-panel1">
		<span class="open-buttton1 closed" title="background color"><br/><br/></span>
		
		<h5>Backgrounds</h5>
		<div  class="bg_patterns">
			<ul class="patterns">
				<li></li>
				<li class="bgs41" title="Biscay flash"></li>
				<li class="bgs42" title="Eastern Blue crystal"></li>
				<li class="bgs43" title="Wasabi blur"></li>
				<li class="bgs44" title="Gray ribbon"></li>
				<li class="bgs45" title="De York blur"></li><li></li>
				<li class="bgs46" title="Key Lime Pie grass"></li>
				<li class="bgs47" title="Limed Spruce checked"></li>
				<li class="bgs48" title="Saffron lights"></li>
				<li class="bgs49" title="Brilliant Rose blur"></li>
				<li class="bgs50" title="Red Damask blur"></li><li></li>
				<li class="bgs51" title="London Hue roses"></li>
				<li class="bgs52" title="Kournikova sun flower"></li>
				<li class="bgs53" title="Shadow Green lilly"></li>
				<li class="bgs54" title="Half Baked scarlet"></li>
				<li class="bgs55" title="Amaranth shade"></li><li></li>
				<li class="bgs56" title="Sweet Pink curve stem"></li>
				<li class="bgs33" title="Remove"><img src="settings/img/remove.png" width="37" height="37" border=0 alt="" ></li>
			</ul>
		</div>
		
		<div  class="bg_patterns">
		<h5>PNG Transparent Patterns</h5>
		<h6>Background Color:</h6> <input  class="color {pickerPosition:'left'}" value="2bafb4" size="5" >
			<ul class="patterns">
				<li></li>
				<li class="img_bgs1" title="Heartine fly"><img src="settings/img/01.jpg" width="37" height="37" border=0 alt="" ></li>
				<li class="img_bgs2" title="scarlet"><img src="settings/img/04.jpg" width="37" height="37" border=0 alt="" ></li>
				<li class="img_bgs3" title="Gift with fly"><img src="settings/img/03.jpg" width="37" height="37" border=0 alt="" ></li>
				<li class="img_bgs4" title="Gift with baloons"><img src="settings/img/02.jpg" width="37" height="37" border=0 alt="" ></li>
				<li class="img_bgs5" title="Heartine with scarlet"><img src="settings/img/07.jpg" width="37" height="37" border=0 alt="" ></li>
				<li></li>
				<li class="img_bgs6" title="Herat Drops"><img src="settings/img/06.jpg" width="37" height="37" border=0 alt="" ></li>
				<li class="img_bgs7" title="lilly"><img src="settings/img/05.jpg" width="37" height="37" border=0 alt="" ></li>
				<li class="img_bgs8" title="Heart line"><img src="settings/img/09.jpg" width="37" height="37" border=0 alt="" ></li>
				<li class="img_bgs9" title="Gift Heart"><img src="settings/img/08.jpg" width="37" height="37" border=0 alt="" ></li>
				<li class="img_bgs10" title="Heart fog"><img src="settings/img/18.jpg" width="37" height="37" border=0 alt="" ></li><li></li>
				
				<li class="img_bgs11" title="flowers"><img src="settings/img/17.jpg" width="37" height="37" border=0 alt="" ></li>
				<li class="img_bgs12" title="Heartines"><img src="settings/img/16.jpg" width="37" height="37" border=0 alt="" ></li>
				<li class="img_bgs13" title="Dotted Heartines"><img src="settings/img/15.jpg" width="37" height="37" border=0 alt="" ></li>
				<li class="img_bgs14" title="heart in heart"><img src="settings/img/14.jpg" width="37" height="37" border=0 alt="" ></li>
				<li class="img_bgs15" title="heart ribbon"><img src="settings/img/13.jpg" width="37" height="37" border=0 alt="" ></li><li></li>
				
				<li class="img_bgs16" title="heart with flower"><img src="settings/img/12.jpg" width="37" height="37" border=0 alt="" ></li>
				<li class="img_bgs17" title="heart"><img src="settings/img/11.jpg" width="37" height="37" border=0 alt="" ></li>
				<li class="img_bgs18" title="Dual heart"><img src="settings/img/10.jpg" width="37" height="37" border=0 alt="" ></li>
				<li class="default" title="Remove"><img src="settings/img/remove.png" width="37" height="37" border=0 alt="" ></li>
			</ul>
		</div>
	
		
		<div  class="colour_patterns">
			<h5>Pre Defined Color</h5>
			<ul class="c_patterns"><li> </li>
				<li  class="c1" title="Tonys Pink" ></li>
				<li  class="c2" title="Eggplant" ></li>
				<li  class="c3" title="Strikemaster" ></li>
				<li  class="c4" title="Cupid"></li>
				<li  class="c5" title="Chelsea Cucumber"></li><li> </li>
				<li  class="c6" title="De York"></li>
				<li  class="c7" title="Deep Blush" ></li>
				<li  class="c8" title="Celeste" ></li>
				<li  class="c9" title="Salem" ></li>
				<li  class="c10" title="Yellow Green" ></li><li> </li>
				<li  class="c12" title="Trendy Pink" ></li>
				<li  class="c13" title="Apricot" ></li>
				<li  class="c14" title="Kabul" ></li>
				<li  class="c15" title="Bouquet" ></li>
				<li  class="c16" title="Brandy Rose" ></li>
				<li  class="c17" title="Highland" ></li>
				<li  class="c18" title="Rodeo Dust" ></li>
				<li  class="c19" title="Teal" ></li>
                <li  class="c21" title="Eastern Blue" ></li>
                <li  class="c22" title="Half baked" ></li>
				
				<li  class="default" title="Reset">Reset</li>
			</ul>
		</div>

</div>
<!----------------------------------------------------------
	/*settings-panel  END	 */
----------------------------------------------------------->


<!-- wrapper -->
<div id="wrapper">
	<div class="clr" id="top-head">
		<!--// Navigation //-->
		<div class="menu_nav">
			<div id="nav-wrap" class="nav-wrap">
				<ul class="arrowunderline" id="nav">
					<li class="home" id="selected">Home</li>
					<li class="wedding">Wedding</li>
					<li class="portfolio">Wedding Album  </li>
					<!-- <li class="features">Features</li> -->
					<li class="gift">Gifts</li>
					<li class="contact">Contact / RSVP</li>	
				</ul>
			</div>
		</div>
		<!--// Navigation End //-->
		<div class="border_pat"></div>
	</div>
	<div class="green_bg">
		<div class="container1">
			<div class="logo_sec">
        		@if(time() < strtotime($couple->marriage_date))
					<div class="clr"><small>Were Getting Married  - {{ date('l, dS F Y', strtotime($couple->marriage_date)) }}</small></div>
				@else
					<div class="clr"><small>Were Got Married on - {{ date('l, dS F Y', strtotime($couple->marriage_date)) }}</small></div>
				@endif
				<div class="clr">
				<div class="middle_heart"></div>
				<p class="groom"><a href="#home" title="Jeff Jacob">{{ $couple->bride->full_name }} </a></p>
				<span></span>
				<p><span class="bride"><a href="#home" title="Nikcy Joe">{{ $couple->groom->full_name }} </a></span></p>
				</div>
			</div>
		</div>
	</div>
	<style>
.circular1 {
	width: 300px;
	height: 300px;
	border-radius: 150px;
	-webkit-border-radius: 150px;
	-moz-border-radius: 150px;
	/*background: url(http://link-to-your/image.jpg) no-repeat;*/
	}
	</style>

	<div class="container">
		<div id="content">
			<div class="card-pattern">
				<!-- Home -->
				<div id="home" class="sub_page">
					<div class="card_content">
						<div class="card_left">
							<div class="clr">
                            <div>
		<ul class="card_image"><li><div><a href="#"><img class="circular1" src="{{ env('picdisplay').$couple->bride->picture }}" alt="" width="344" height="289"/></a></div>
		</li></ul></div>
                          
                            </div>
							<div class="card_heading">About {{ $couple->bride->full_name }}</div>
							<div class="clr">
								<p>{{ $couple->bride->about }}</p>
							</div>
						</div>
						<div class="card_middle"><div class="card_circle"><a href="#">+</a></div></div>
						<div class="card_right">
							<div>
		<ul class="card_image"><li><div><a href="#"><img  class="circular1" width="344" height="289" src="{{ env('picdisplay').$couple->groom->picture }}" alt=""/></a></div></li></ul></div>
							<div class="card_heading">About {{ $couple->groom->full_name }}</div>
							<div class="clr">
								<p>{{ $couple->groom->about }}</p>
							</div>
						</div>
						<div class="clr"></div>
						<div class="dot_line">
						<div class="lines_dot">
						<div class="heart">&nbsp;</div>
						</div>
						</div>
						<div class="center" id="venue">
							<div class="date"><?php echo date('j. F Y', strtotime($couple->marriage_date)) ?></div>
							<!-- <div class="time"><span>08.00</span> AM - <span>12.00</span> AM</div> -->
							<div class="location">{{ $couple->wedding_location }}</div>
						</div>
					</div>
					<div class="clr"></div>
				</div>
				<!-- //Home -->
			
				<!-- Wedding -->
				<div id="wedding" class="sub_page">
					<div class="card_content">
						<div class="heading">wedding</div>
						<div class="clr">
							<h6>The ceremony and reception were both held at <br>
							<span>{{ $couple->location }} on <i><?php echo date('F j, Y') ?></i>.</span></h6>
						</div>
						<div class="clr hline">&nbsp;</div>
						<div class="wedding">
							<ul class="list">
								@foreach($couple->schedules as $s)
								<li>
									<span class="head">{{ $s->name }}</span><br /><br />
									{!! $s->info !!}<br />
									<strong style="font-weight:bold">{{ date('d, F Y', strtotime($s->date)) }} <small>{{ $s->time }}</small></strong> <br />
									
								</li>
								@endforeach
							</ul>
						</div>
					</div>
					<div class="clr"></div>
				</div>
				<!-- //Wedding -->
				
					<!-- Portfolio -->
				<div id="portfolio" class="sub_page">
					<div class="card_content">
							<div class="heading">Wedding Album</div>
						<div class="clr">
							<!-- <h6>These sites are a sample of the client work undertaken by us.
							<span>Click a site to learn more about its development.</span></h6> -->
						</div>
						<div class="clr hline">&nbsp;</div>
						<div class="clr">
							<!-- <ul id="portfolio-filter">	
								<li><a href="#" class="current" data-filter="*">View All</a></li>
								<li><a href="#" data-filter=".webdesign">Bridesmaids</a></li>
								<li><a href="#" data-filter=".print">Ceremony</a></li>
								<li><a href="#" data-filter=".video">Getting Married</a></li>
								<li><a href="#" data-filter=".photoghraphy">Honeymoon</a></li>
							</ul> -->
							<ul id="portfolio-list">
								
								@foreach($galleryPixs as $p)
								<li class="webdesign video">
									<a href="{{ env('picdisplay').$p }}" title="Blessings Of Marriage" rel="prettyPhoto[gallery]" class="viewDetail lightbox">
									<img src="{{ env('picdisplay').$p }}"   alt=" " />
									<!-- <h5>Blessings Of Marriage</h5> -->
									</a>                  		
								</li>                     
								@endforeach   
								<!-- <li class="photoghraphy print">
								  <a href="{{ asset('templates/slide/images/assets/small/portfolio_img2.jpg')}}" title="Consectetur adipiscing elit ipsum" rel="prettyPhoto[gallery]" class="viewDetail lightbox">
									<img src="{{ asset('templates/slide/images/assets/small/portfolio_img2.jpg')}}" alt=" "/>
									<h5>Consectetur adipiscing elit ipsum</h5>
								  </a>                        
								</li>
								<li class="webdesign print">
									<a href="{{ asset('templates/slide/images/assets/small/portfolio_img3.jpg')}}" title="Consectetur adipiscing elit"  rel="prettyPhoto[gallery]"   class="viewDetail lightbox ">
									<img src="images/assets/small/portfolio_img3.jpg" alt=" "/>
									<h5>Consectetur adipiscing elit </h5>
									</a>                        
								</li>
								<li class="photography print">
									<a href="images/assets/small/portfolio_img4.jpg" title="Adipiscing elit ipsum nisl"  rel="prettyPhoto[gallery]"   class="viewDetail lightbox ">
									<img src="images/assets/small/portfolio_img4.jpg" alt=" "/>
									<h5>Adipiscing elit ipsum nisl</h5>
									</a>                        
								</li>
								<li class="webdesign video">
									<a href="images/assets/small/portfolio_img5.jpg" title="Sectetur adipiscing elit ipsum"  rel="prettyPhoto[gallery]" class="viewDetail lightbox">
									<img src="images/assets/small/portfolio_img5.jpg" alt=" "/>
									<h5>Sectetur adipiscing elit ipsum </h5>
									</a>                       
								</li>
								<li class="photoghraphy print">
								<a href="images/assets/small/portfolio_img6.jpg" title="Ctetur adipiscing elit ipsum"  rel="prettyPhoto[gallery]"   class="viewDetail lightbox ">
									<img src="images/assets/small/portfolio_img6.jpg" alt=" "/>
									<h5>Ctetur adipiscing elit ipsum</h5>
								  </a>                      
								</li>
								<li class="photoghraphy print">
								<a href="images/assets/small/portfolio_img7.jpg" title="Nsectetur adipiscing elit ipsum"  rel="prettyPhoto[gallery]"   class="viewDetail lightbox ">
									<img src="images/assets/small/portfolio_img7.jpg" alt=" "/>
									<h5>Nsectetur adipiscing elit ipsum</h5>
								  </a>                      
								</li>
								<li class="webdesign video">
							<a href="images/assets/small/portfolio_img8.jpg" title="Ectetur adipiscing elit ipsum nisl"  rel="prettyPhoto[gallery]"   class="viewDetail lightbox ">
									<img src="images/assets/small/portfolio_img8.jpg" alt=" "/>
									<h5>Ectetur adipiscing elit ipsum nisl </h5>
								  </a>						
								</li> -->
							</ul>
						</div>			
					</div>
					<div class="clr"></div>
				</div>
				<!-- //Portfolio -->
				
			
			
				<!-- Gift -->
				<div id="gift" class="sub_page">
					<div class="card_content">
							<div class="heading">Gifts Registry</div>
						<div class="clr">
							<!-- <h6>Lorem ipsum dolor sit amet, habitasse pretium dolor sociis.
							<span>Nulla et facilisis interdum elit amet erat, consectetuer condimentum eaque, ante maecenas. </span></h6> -->
						</div>	
						<div class="clr hline">&nbsp;</div>
						<div class="register-top">	
							<div class="register">We Are Registered At…</div>
							<div class="clr">
								<div>
									<ul class="logo_image">
										<li><div ><a href="#"><img src="{{ asset('templates/slide/images/gift_logo1.png')}}" alt="" /></a></div></li>
										<li><div><a href="#"><img src="{{ asset('templates/slide/images/gift_logo2.png')}}" alt="" /></a></div></li>
										<li><div><a href="#"><img src="{{ asset('templates/slide/images/gift_logo3.png')}}" alt="" /></a></div></li>
										<li><div><a href="#"><img src="{{ asset('templates/slide/images/gift_logo2.png')}}" alt="" /></a></div></li>
										<li><div><a href="#"><img src="{{ asset('templates/slide/images/gift_logo3.png')}}" alt="" /></a></div></li>
										<li><div ><a href="#"><img src="{{ asset('templates/slide/images/gift_logo1.png')}}" alt="" /></a></div></li>


										
									</ul>
								</div>
							</div>	  
							<div class="dot_line padtop50">
							<div class="lines_dot">
							<div class="heart">&nbsp;</div>
							</div>
							</div>	
							<div class="clr"></div>
						</div>			
					</div>
				</div> 
				<!-- //Gift -->	
			
			
			
			<!-- contact -->
				<div id="contact" class="sub_page">
					<div class="card_content">
							<div class="heading">Contact / RSVP</div>
						<div class="clr">
							<h6>If you have any questions or just want to get in touch with us,
							<span>feel free to do so through this page, Our Wedding Location Below.</span></h6>
						</div>
						<div class="clr hline">&nbsp;</div>
						<div class="clr" >
							<iframe src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=UK&amp;aq=&amp;sll=43.834527,-86.308594&amp;sspn=86.91721,186.152344&amp;ie=UTF8&amp;hq=&amp;hnear=United+Kingdom&amp;t=m&amp;ll=54.797518,-2.735596&amp;spn=0.49081,3.356323&amp;z=8&amp;output=embed"></iframe>
						</div>			
						<div class="left">
							<div class="clr">
								<div id="respond">
									<h2>Tell us something</h2>
									<div class="pad-top5">If you received an invitation (and hey, why else would you be here?), 
								we’d like to plan in advance.</div> 								
									<div id="post_message" class="post_message"></div>
									<div class="boxBody">			  
										<div class="desc">
											<form id="RSVPFORM" action="{{ route('contact-us') }}" method="post">

											{!! csrf_field() !!}

                    						<input type="hidden" name="couple_id" value="{{ $couple->id }}"> 

											<div><div class="clr"></div><br>
											<input name="name" type="text" class="required" placeholder="Name *" />
											</div>
											<div>
											<div class="clr"></div><br>
											<input name="email" type="text" class="required email" placeholder="E-mail *" />		
											</div>
											<div>
											<div class="clr"></div><br>

											<input name="phone" type="text" class="required phone" placeholder="Phone  *"  />		
											</div>
											<div>
											<div class="clr"></div><br>

											<input name="guests" type="text"  class="required phone" placeholder="Number Of Guests *"  />		
											</div>
											<div class="textarea">
											<div class="clr"></div><br>		
											<textarea name="comments" rows="6" cols="60" class="required" placeholder="Message  *"></textarea></div>
											<div><input id="submitBtn" value="Submit"  name="submit" type="submit" class="btn submitBtn" /></div>
											</form>	
											<div id="flash" style="color:red"></div>
										</div><!--END desc show--> 
										<!--END desc-->	
									</div>					
									<div  class="clr"></div>
								</div>
							</div>
						</div>
						<!--//left end //-->
						<div class="right">
							<div class="clr">
								<h2>Wedding Location</h2>
								<div class="clr">
									<div class="clr">
										<div class="input-box1">Address</div>
										<span>{{ $couple->location }}</span>
									</div>
									<div class="clr"> &nbsp;</div>
									<div class="clr">
										<div class="input-box1">Phone</div>
										<span>{{ $couple->user->phone }}</span>
									</div>
									<div class="clr">
										<div class="input-box1">E-mail</div>
										<span><a href="#">{{ $couple->user->email }}</a></span>  
									</div>
								</div>
								<!-- <div class="clr">
									<div class="pad-top5"></div>
									<h2>Latest Tweet</h2>
									<div class="twitter"></div>
								</div> -->
								
								<div class="clr">
								<ul class="contact_icon">
								<li><a href="#"><img src="images/social/card_facebook.png" width="42" height="42" alt=""/></a></li>
								<li><a href="#"><img src="images/social/card_linkedin.png" width="42" height="42" alt="" /></a></li>
								<li><a href="#"><img src="images/social/dribble.png" width="42" height="42" alt=""/></a></li>
								<li><a href="#"><img src="images/social/google.png" width="42" height="42" alt="" /></a></li>
								<li><a href="#"><img src="images/social/pinterest.png" width="42" height="42" alt="" /></a></li>
								</ul>
								</div>
								
																
							</div>
						</div><!--//clr end //-->
						<div class="clr"></div>
					</div>
				</div>
				<!-- contact end -->
				<div class="clr"></div>
			</div><!--card pattern end -->
			<div class="clr "></div>
		</div>
		<div class="bottom-shade"></div>
	</div>
</div>
	<!-- JS
  ================================================== -->	

<!-- include jQuery library -->
<script type="text/javascript" src="{{ asset('templates/slide/js/jquery-1.10.2.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('templates/slide/js/jquery.prettyPhoto.js')}}"></script>
<script type="text/javascript" src="{{ asset('templates/slide/js/jquery.tweet.min.js')}}"></script>
<!--form validation these scripts must for this kind of validation-->
<script src="{{ asset('templates/slide/js/jquery.validate.min.js')}}" type="text/javascript"></script>
<!-- Portfolio and Hover Image adipoli Script -->
<script type="text/javascript" src="{{ asset('templates/slide/js/jquery.isotope.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('templates/slide/js/jquery.adipoli.min.js') }}"></script>


<!-- include custom js -->
<script type="text/javascript" src="{{ asset('templates/slide/js/custom.js')}}"></script>
<!-- settings-panel Demo Purpose Script -->
<script type="text/javascript" src="{{ asset('templates/slide/settings/settings/settings.js')}}"></script>
<script type="text/javascript" src="{{ asset('templates/slide/settings/settings/jscolor.js')}}"></script>

<!-- include retina js -->
<script type="text/javascript" src="{{ asset('templates/slide/js/retina-1.1.0.min.js')}}"></script>

    <script src="{{ asset('admin/jquery.form.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin/notify.min.js') }}" type="text/javascript"></script>


    <script>
 		$('#RSVPFORM').ajaxForm({ 
                    headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
                    beforeSubmit:btn,
                    success:showResponse
            }); 

                    function btn(){
                        console.log('Logging things')
                    }

                    function showResponse(){
                        console.log('Am here for you.')
                        $('#flash').html('Your comments was successfully submitted')

                        setTimeout(function(){
                            $('#flash').hide()
                        }, 3000);

                        $("#RSVPFORM")[0].reset()

                    }
    </script>

  <!--Container / wrapper end -->	
</body>
</html>
