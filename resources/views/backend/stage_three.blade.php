@extends('layouts.admin.admin')

@section('content')


<script src="//rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>



<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Forms Stuff</a></li>
    <li><a href="#">Form Layout</a></li>
    <li class="active">Two Column</li>
</ul>


 

 <script>
 

 </script>
 

 <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">

                            {!! csrf_field() !!}
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add pictures</strong></h3>
                                    <ul class="panel-controls">
                                        <li><a class="panel-remove" href="#"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <!-- <p>This is non libero bibendum, scelerisque arcu id, placerat nunc. Integer ullamcorper rutrum dui eget porta. Fusce enim dui, pulvinar a augue nec, dapibus hendrerit mauris. Praesent efficitur, elit non convallis faucibus, enim sapien suscipit mi, sit amet fringilla felis arcu id sem. Phasellus semper felis in odio convallis, et venenatis nisl posuere. Morbi non aliquet magna, a consectetur risus. Vivamus quis tellus eros. Nulla sagittis nisi sit amet orci consectetur laoreet. Vivamus volutpat erat ac vulputate laoreet. Phasellus eu ipsum massa.</p> -->
                                </div>
                                <div class="panel-body">                                                                        
                                             
                                             <h2> Engagement Details  </h2> 
                                    <div class="row" id="demo">


                                        
                                            <div class="col-md-12">
                                                
                                                <div class="form-group" id="CoupPics">                                        
                                                    <label class="col-md-3 control-label">Couple Pictures</label>
                                                    <div class="col-md-9">
                                                        <div class="panel panel-default">
                                                          <div class="panel-body">
                                                                <h3><span class="fa fa-download"></span> Couple Pictures</h3>
                                                                <!-- <p>Add form with class <code>dropzone</code> to get dropzone box</p> -->
                                                                   <form  id="CoupleGallery" action="{{ route('couple-gallery') }}" method="post" enctype="multipart/form-data">
                                                                        <input type="hidden" name="type" value="couples">
                                                                        {!! csrf_field() !!}
                                                                        <div class="form-group" id="couplePicture">
                                                                            <div class="col-md-12">
                                                                                <label>Select up to 3 pictures</label>
                                                                                <input type="file" multiple name="couples[]" class="file" data-preview-file-type="any" required/>
                                                                            </div>
                                                                        </div>
                                                                        <div id="flash" style="color:red"></div>
                                                                    </form>
                                                          </div>
                                                        </div>
                                                        <!-- <span class="help-block">Click on input field to get datepicker</span> -->
                                                    </div>
                                                </div>



                                                 <div class="form-group">                                        
                                                    <label class="col-md-3 control-label">Gallery Pictures</label>
                                                    <div class="col-md-9">
                                                        <div class="panel panel-default">
                                                          <div class="panel-body">
                                                                <h3><span class="fa fa-download"></span> Couple Gallery</h3>
                                                                <!-- <p>Add form with class <code>dropzone</code> to get dropzone box</p> -->
                                                                   <form id="WeddingGallery" action="{{ route('couple-gallery') }}" method="post" enctype="multipart/form-data">
                                                                        <input type="hidden" name="type" value="gallery">
                                                                        {!! csrf_field() !!}
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <label>Add more than 8 pictures</label>
                                                                                <input type="file" multiple name="gallery[]" class="file" data-preview-file-type="any" required/>
                                                                            </div>
                                                                        </div>
                                                                        <div id="flashing" style="color:red"></div>

                                                                    </form>
                                                          </div>
                                                        </div>
                                                        <!-- <span class="help-block">Click on input field to get datepicker</span> -->
                                                    </div>
                                                </div>

                                                
                                            </div>
                                            
                                    </div>

<!-- <button id="testNotify">Test Notify</button> -->
                                   


                                    <br><br><br>
                                          

                                


                    <a class="btn btn-success pull-right btn-block" href="{{ route('couple-template', ['id'=>1]) }}">Proceed</a>                 


                                </div>
                                <div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right">Pro</button>
                                </div>
                            </div>
                            
                        </div>
                    </div>   


                </div>


                
        <script type="text/javascript" src="{{ asset('admin/jquery.form.js') }}"></script>


                <script>

                    $('#CoupleGallery').ajaxForm({ 
                            headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
                            beforeSubmit:btn,
                            success:showResponse
                    }); 

                    function btn(){
                        console.log('Logging things')
                    }

                    function showResponse(){
                        console.log('Am here for you.')
                       
                        $('#flash').html('Uploading');
                        setTimeout(function(){ 
                            $('#flash').html('');
                           $("#flash").notify("Pictures uploaded successfully");
                        }, 3000);


                    }

                   
                    $('#WeddingGallery').ajaxForm({ 
                            headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
                            beforeSubmit:btns,
                            success:showResponses
                    }); 

                    function btns(){
                        $(this).val('Please wait...')
                         $('#flashing').html('Uploading');

                    }

                    function showResponses(){
                        console.log('Gallery pics Uploaded')

                        setTimeout(function(){ 
                           
                            $('#flashing').html('');
                           $("#flashing").notify("Pictures uploaded successfully");
                        }, 3000);
                    }
                </script>

       

@stop
    