@extends('layouts.admin.admin')

@section('content')

<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Forms Stuff</a></li>
    <li><a href="#">Form Layout</a></li>
    <li class="active">Two Column</li>
</ul>

 
 <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form class="form-horizontal" action="" method="post">

                            {!! csrf_field() !!}
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Two Column</strong> Layout</h3>
                                    <ul class="panel-controls">
                                        <li><a class="panel-remove" href="#"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <!-- <p>This is non libero bibendum, scelerisque arcu id, placerat nunc. Integer ullamcorper rutrum dui eget porta. Fusce enim dui, pulvinar a augue nec, dapibus hendrerit mauris. Praesent efficitur, elit non convallis faucibus, enim sapien suscipit mi, sit amet fringilla felis arcu id sem. Phasellus semper felis in odio convallis, et venenatis nisl posuere. Morbi non aliquet magna, a consectetur risus. Vivamus quis tellus eros. Nulla sagittis nisi sit amet orci consectetur laoreet. Vivamus volutpat erat ac vulputate laoreet. Phasellus eu ipsum massa.</p> -->
                                </div>
                                <div class="panel-body">                                                                        
                                             
                                             <h2> Engagement Details <small>leave empty if you dont want this section to appear up front</small> <small class='pull-right' > <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Add</button> </small> </h2> 
                                    <div class="row collapse" id="demo">
                                        
                                            <div class="col-md-6">
                                                
                                                <div class="form-group">                                        
                                                    <label class="col-md-3 control-label">Engagment Date</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                            <input type="text" name="date1"  class="form-control datepicker" required>                                            
                                                        </div>
                                                        <span class="help-block">Click on field to get date</span>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">                                        
                                                    <label class="col-md-3 control-label">Engagement Time</label>
                                                    <div class="col-md-9 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                                            <input type="text" name="time1" class="form-control timepicker" required>
                                                        </div>            
                                                        <!-- <span class="help-block">Password field sample</span> -->
                                                    </div>
                                                </div>
                                                
                                                
                                                
                                            </div>
                                            
                                            <div class="col-md-6">
                                                
                                                 <div class="form-group">                                        
                                                    <label class="col-md-3 control-label">Location</label>
                                                    <div class="col-md-9 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                                            <input type="text" name="location1" placeholder='e.g. Lagos' class="form-control" required>
                                                        </div>            
                                                        <!-- <span class="help-block">Password field sample</span> -->
                                                    </div>
                                                </div>
                                                
                                               <div class="form-group">
                                                    <label class="col-md-3 control-label">More Info</label>
                                                    <div class="col-md-9 col-xs-12">                                            
                                                        <textarea rows="5" name="info1" class="form-control" required></textarea>
                                                        <!-- <span class="help-block">Default textarea field</span> -->
                                                    </div>
                                                </div>
                                            </div>
                                    </div>

                                    
                                    <br><br><br>
                                             <h2> Wedding Details <small>leave empty if you dont want this section to appear up front</small> <small class='pull-right' > <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#dem">Add</button> </small> </h2> 

                                     <div class="row collapse" id="dem">
                                        
                                             <!-- <h2> Wedding Details <small class='pull-right' > <a href="#">hide </a> </small> </h2>  -->
                                            <div class="col-md-6">
                                                
                                                <div class="form-group">                                        
                                                    <label class="col-md-3 control-label">Wedding Date</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                            <input type="text" name="date2"required class="form-control datepicker">                                            
                                                        </div>
                                                        <span class="help-block">Click on field to get date</span>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">                                        
                                                    <label class="col-md-3 control-label">Wedding Time</label>
                                                    <div class="col-md-9 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                                            <input type="text" name="time2" placeholder='e.g. 8am' class="form-control timepicker" required>
                                                        </div>            
                                                        <!-- <span class="help-block">Password field sample</span> -->
                                                    </div>
                                                </div>
                                                
                                                
                                            </div>
                                            
                                            <div class="col-md-6">
                                                
                                                 <div class="form-group">                                        
                                                    <label class="col-md-3 control-label">Location</label>
                                                    <div class="col-md-9 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                                            <input type="text" name="location2" placeholder='e.g. Lagos' class="form-control" required>
                                                        </div>            
                                                        <!-- <span class="help-block">Password field sample</span> -->
                                                    </div>
                                                </div>
                                                
                                               <div class="form-group">
                                                    <label class="col-md-3 control-label">More Info</label>
                                                    <div class="col-md-9 col-xs-12">                                            
                                                        <textarea rows="5" name="info2" class="form-control" required></textarea>
                                                        <!-- <span class="help-block">Default textarea field</span> -->
                                                    </div>
                                                </div>
                                            </div>
                                    </div>



                                    <br><br><br>
                                             <h2> Reception Details <small>leave empty if you dont want this section to appear up front</small> <small class='pull-right' > <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo3">Add</button> </small> </h2> 

                                     <div class="row collapse" id="demo3">
                                        
                                            <div class="col-md-6">
                                                
                                                <div class="form-group">                                        
                                                    <label class="col-md-3 control-label">Reception Date</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                            <input type="text" name="date3" required class="form-control datepicker">                                            
                                                        </div>
                                                        <span class="help-block">Click on field to get date</span>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">                                        
                                                    <label class="col-md-3 control-label">Reception Time</label>
                                                    <div class="col-md-9 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                                            <input type="text" name="time3" placeholder='e.g. 8am' class="form-control timepicker" required>
                                                        </div>            
                                                        <!-- <span class="help-block">Password field sample</span> -->
                                                    </div>
                                                </div>
                                                
                                                
                                                
                                            </div>
                                            
                                            <div class="col-md-6">
                                                
                                                 <div class="form-group">                                        
                                                    <label class="col-md-3 control-label">Location</label>
                                                    <div class="col-md-9 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                                            <input type="text" name="location3" placeholder='e.g. Lagos' required class="form-control">
                                                        </div>            
                                                        <!-- <span class="help-block">Password field sample</span> -->
                                                    </div>
                                                </div>
                                                
                                               <div class="form-group">
                                                    <label class="col-md-3 control-label">More Info</label>
                                                    <div class="col-md-9 col-xs-12">                                            
                                                        <textarea rows="5" name="info3" class="form-control" required></textarea>
                                                        <!-- <span class="help-block">Default textarea field</span> -->
                                                    </div>
                                                </div>
                                            </div>
                                    </div>



                                




                                </div>
                                <div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>

@stop
    