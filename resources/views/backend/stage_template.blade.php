@extends('layouts.admin.admin')

@section('content')

    <link href="{{ asset('admin/slider/ninja-slider.css') }}" rel="stylesheet" />
    <script src="{{ asset('admin/slider/ninja-slider.js') }}"></script>
    <link href="{{ asset('admin/slider/thumbnail-slider.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('admin/slider/thumbnail-slider.js') }}" type="text/javascript"></script>




<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Forms Stuff</a></li>
    <li><a href="#">Form Layout</a></li>
    <li class="active">Two Column</li>
</ul>

 
 <div class="page-content-wrap">
                
    <div class="row">
        <div class="col-md-12">
        
      <!--   <div class="form-group">
            @foreach($templates as $template)
            <button class="btn btn-default" onclick="showTemp({{ $template->id }});return false;" type="button">{{ $template->name }}</button>
            @endforeach                                                             
        </div> -->
          <div class="form-groupm col-md-5 col-md-offset-3">
            @foreach($templates as $t)
            <!-- <button class="btn btn-default"  type="button">{{ $template->name }}</button> -->
            <a class="btn btn-default" href="{{ route('couple-template', ['id'=>$t->id]) }}">{{ $t->name }}</a>
            @endforeach                                                             
        </div>
        
    <!--start-->
            <div class="col-md-5 col-md-offset-3">
                   <br><br> <button onclick="chooseTemp({{ $temp->id }});return false;"  class="btn btn-info btn-block">Choose {{ $temp->name }}</button><br><br>
            </div>



    <div  style="width:1000px;margin:80px auto;" id="ContDiv">
        <div id="ninja-slider" style="float:left;">
            <div class="slider-inner">
                <ul>
                    @foreach($temp->pics as $t)
                    <li><a class="ns-img" href="{{ asset('uploads/temps/'.$t->picture)}}"></a></li>
                    @endforeach
                   <!--  <li><a class="ns-img" href="{{ asset('admin/slider/img/2.jpg') }}"></a></li>
                    <li><a class="ns-img" href="{{ asset('admin/slider/img/3.jpg') }}"></a></li>
                    <li><a class="ns-img" href="{{ asset('admin/slider/img/4.jpg')}}"></a></li>
                    <li><a class="ns-img" href="{{ asset('admin/slider/img/5.jpg') }}"></a></li>
                    <li><a class="ns-img" href="{{ asset('admin/slider/img/6.jpg') }}"></a></li>
                    <li><a class="ns-img" href="{{ asset('admin/slider/img/7.jpg') }}"></a></li>
                    <li><a class="ns-img" href="{{ asset('admin/slider/img/8.jpg') }}"></a></li>
                    <li><a class="ns-img" href="{{ asset('admin/slider/img/9.jpg') }}"></a></li> -->
                   
                </ul>
                <div class="fs-icon" title="Expand/Close"></div>
            </div>
        </div>
        <div id="thumbnail-slider" style="float:left;">
            <div class="inner">
                <ul>
                    @foreach($temp->pics as $t)
                    <li>
                        <a class="thumb" href="{{ asset('uploads/temps/'.$t->picture)}}"></a>
                    </li>
                    @endforeach
                    <!-- <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/2.jpg') }}"></a>
                    </li>
                    <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/3.jpg') }}"></a>
                    </li>
                    <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/4.jpg') }}"></a>
                    </li>
                    <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/5.jpg') }}"></a>
                    </li>
                    <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/6.jpg') }}"></a>
                    </li>
                    <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/7.jpg') }}"></a>
                    </li>
                    <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/8.jpg') }}"></a>
                    </li>
                    <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/9.jpg') }}"></a>
                    </li>
                   -->
                </ul>
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>

     <div class="col-md-5 col-md-offset-3">
                   <button onclick="chooseTemp({{ $temp->id }});return false;" class="btn btn-info btn-block">Choose {{ $temp->name }}</button> <br>
            </div>
    <!--end-->
            
        </div>
    </div>
</div>

<script>
    function chooseTemp (id) {
        console.log(id)
        pageLoadingFrame("show");
        
        var token = '{{ csrf_token() }}'
        var url = "{{ route('choose-template') }}"
        $.ajax
        ({
            type: "POST",
            url: url,
            data: ({ rnd : Math.random() * 100000, _token:token, template_id:id }),
            success: function(response){
                 console.log(response)
                 // pageLoadingFrame("hide");

            window.location = response;


                  

            }
        });

    }
</script>
       

@stop
    