@extends('layouts.admin.admin')

@section('content')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">


<style>
    .wizard {
    margin: 20px auto;
    background: #fff;
}

    .wizard .nav-tabs {
        position: relative;
        margin: 40px auto;
        margin-bottom: 0;
        border-bottom-color: #e0e0e0;
    }

    .wizard > div.wizard-inner {
        position: relative;
    }

.connecting-line {
    height: 2px;
    background: #e0e0e0;
    position: absolute;
    width: 80%;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 50%;
    z-index: 1;
}

.wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
    color: #555555;
    cursor: default;
    border: 0;
    border-bottom-color: transparent;
}

span.round-tab {
    width: 70px;
    height: 70px;
    line-height: 70px;
    display: inline-block;
    border-radius: 100px;
    background: #fff;
    border: 2px solid #e0e0e0;
    z-index: 2;
    position: absolute;
    left: 0;
    text-align: center;
    font-size: 25px;
}
span.round-tab i{
    color:#555555;
}
.wizard li.active span.round-tab {
    background: #fff;
    border: 2px solid #5bc0de;
    
}
.wizard li.active span.round-tab i{
    color: #5bc0de;
}

span.round-tab:hover {
    color: #333;
    border: 2px solid #333;
}

.wizard .nav-tabs > li {
    width: 25%;
}

.wizard li:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 0;
    margin: 0 auto;
    bottom: 0px;
    border: 5px solid transparent;
    border-bottom-color: #5bc0de;
    transition: 0.1s ease-in-out;
}

.wizard li.active:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 1;
    margin: 0 auto;
    bottom: 0px;
    border: 10px solid transparent;
    border-bottom-color: #5bc0de;
}

.wizard .nav-tabs > li a {
    width: 70px;
    height: 70px;
    margin: 20px auto;
    border-radius: 100%;
    padding: 0;
}

    .wizard .nav-tabs > li a:hover {
        background: transparent;
    }

.wizard .tab-pane {
    position: relative;
    padding-top: 50px;
}

.wizard h3 {
    margin-top: 0;
}

@media( max-width : 585px ) {

    .wizard {
        width: 90%;
        height: auto !important;
    }

    span.round-tab {
        font-size: 16px;
        width: 50px;
        height: 50px;
        line-height: 50px;
    }

    .wizard .nav-tabs > li a {
        width: 50px;
        height: 50px;
        line-height: 50px;
    }

    .wizard li.active:after {
        content: " ";
        position: absolute;
        left: 35%;
    }
}
</style>


                <!-- END X-NAVIGATION VERTICAL -->                   
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Forms Stuff</a></li>
                    <li><a href="#">Form Layout</a></li>
                    <li class="active">One Column</li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
    
        <div class="wizard">
                            
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-folder-open"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-picture"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
</div>
                            
                            <form class="form-horizontal" id="commentForm" method="post" action="" enctype="multipart/form-data"> 

                            {{ csrf_field() }}
                            
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Couple, Bride and Groom</strong> Details</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
    <div class="error" style="color:red"></div>

                                    <!-- <p>This is non libero bibendum, scelerisque arcu id, placerat nunc. Integer ullamcorper rutrum dui eget porta. Fusce enim dui, pulvinar a augue nec, dapibus hendrerit mauris. Praesent efficitur, elit non convallis faucibus, enim sapien suscipit mi, sit amet fringilla felis arcu id sem. Phasellus semper felis in odio convallis, et venenatis nisl posuere. Morbi non aliquet magna, a consectetur risus. Vivamus quis tellus eros. Nulla sagittis nisi sit amet orci consectetur laoreet. Vivamus volutpat erat ac vulputate laoreet. Phasellus eu ipsum massa.</p> -->
                                </div>
                                <div class="panel-body">  

                                 <div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">Marriage Date</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input name="marriage_date" type="text" class="form-control datepicker" required>                                            
                                            </div>
                                            <span class="help-block">Click on field to get date</span>
                                        </div>
                                    </div> 

                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Our Love story</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <textarea class="form-control" rows="5" name="love_story" required></textarea>
                                            <!-- <span class="help-block">Default textarea field</span> -->
                                        </div>
                                    </div>  


                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Origin of Love</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="love_origin" required='required' />
                                            </div>                                            
                                            <span class="help-block">Where you met</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Wedding Location</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="location" required />
                                            </div>                                            
                                            <!-- <span class="help-block">This is sample of text field</span> -->
                                        </div>
                                    </div>
                                    
                                   
                                    

                                    <br><br><br>
                                    <div class="row">
                                        
                                        <div class="col-md-6">

                                            <h3>Groom Details</h3>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Display Name</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input type="text" class="form-control" name="groom_dn" required>
                                                    </div>                                            
                                                    <span class="help-block">Single Name e.g. Smith</span>
                                                </div>
                                            </div>


                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Full Name</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input type="text" class="form-control" name="groom_fn" required>
                                                    </div>                                            
                                                    <!-- <span class="help-block">This is sample of text field</span> -->
                                                </div>
                                            </div>
                                            
                                           
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">About</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <textarea rows="5" class="form-control" name="groom_about" required></textarea>
                                                    <span class="help-block">Short Description, 3 sentences max.</span>
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Toast</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <textarea rows="5" class="form-control" name="groom_toast" required></textarea>
                                                    <span class="help-block">Short Toast, 3 sentences max.</span>
                                                </div>
                                            </div>

                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Groom Single Picture</label>
                                                <div class="col-md-9">                                                                                                                                        
                                                    <!-- <a class="file-input-wrapper btn btn-default  fileinput btn-primary"><span>Browse file</span> -->
                                                    <input type="file" title="Browse file" id="filename" name="groom_single" class="fileinput btn-primary" required></a>
                                                    <!-- <span class="help-block">Input type file</span> -->
                                                </div>
                                            </div>
                                          

                                        </div>

                                <div class="col-md-6">

                                            <h3>Bride Details</h3>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Display Name</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input type="text" class="form-control" name="bride_dn" required>
                                                    </div>                                            
                                                    <span class="help-block">Single Name e.g. Smith</span>
                                                </div>
                                            </div>


                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Full Name</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input type="text" class="form-control" name="bride_fn" required>
                                                    </div>                                            
                                                    <!-- <span class="help-block">This is sample of text field</span> -->
                                                </div>
                                            </div>
                                            
                                           
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">About</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <textarea rows="5" class="form-control" name="bride_about" required></textarea>
                                                    <span class="help-block">Short Description, 3 sentences max.</span>
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Toast</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <textarea rows="5" class="form-control" name="bride_toast" required></textarea>
                                                    <span class="help-block">Short Toast, 3 sentences max.</span>
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Bride Single Picture</label>
                                                <div class="col-md-9">                                                                                                                                        
                                                    <!-- <a class="file-input-wrapper btn btn-default  fileinput btn-primary"><span>Browse file</span> -->
                                                    <input type="file" title="Browse file" id="bride_single" name="bride_single" class="fileinput btn-primary" required></a>
                                                    <!-- <span class="help-block">Input type file</span> -->
                                                </div>
                                            </div>
                                            
                                        </div>
                                        



                                       
                                        
                                    </div>








                                </div>
                                <div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="pages-login.html" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <script type="text/javascript" src="{{ asset('admin/js/actions.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>

        <script>
        $("#commentForm").validate();

        </script>

@stop
    