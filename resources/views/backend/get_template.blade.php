<!--start-->

    <div style="width:1000px;margin:80px auto;" id="ContDiv">
        <div id="ninja-slider" style="float:left;">
            <div class="slider-inner">
                <ul>
                    @foreach($template->pics as $t)
                    <li><a class="ns-img" href="{{ asset('uploads/temps/'.$t->picture)}}"></a></li>
                    @endforeach
                   <!--  <li><a class="ns-img" href="{{ asset('admin/slider/img/2.jpg') }}"></a></li>
                    <li><a class="ns-img" href="{{ asset('admin/slider/img/3.jpg') }}"></a></li>
                    <li><a class="ns-img" href="{{ asset('admin/slider/img/4.jpg')}}"></a></li>
                    <li><a class="ns-img" href="{{ asset('admin/slider/img/5.jpg') }}"></a></li>
                    <li><a class="ns-img" href="{{ asset('admin/slider/img/6.jpg') }}"></a></li>
                    <li><a class="ns-img" href="{{ asset('admin/slider/img/7.jpg') }}"></a></li>
                    <li><a class="ns-img" href="{{ asset('admin/slider/img/8.jpg') }}"></a></li>
                    <li><a class="ns-img" href="{{ asset('admin/slider/img/9.jpg') }}"></a></li> -->
                   
                </ul>
                <div class="fs-icon" title="Expand/Close"></div>
            </div>
        </div>
        <div id="thumbnail-slider" style="float:left;">
            <div class="inner">
                <ul>
                    @foreach($template->pics as $t)
                    <li>
                        <a class="thumb" href="{{ asset('uploads/temps/'.$t->picture)}}"></a>
                    </li>
                    @endforeach
                    <!-- <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/2.jpg') }}"></a>
                    </li>
                    <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/3.jpg') }}"></a>
                    </li>
                    <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/4.jpg') }}"></a>
                    </li>
                    <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/5.jpg') }}"></a>
                    </li>
                    <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/6.jpg') }}"></a>
                    </li>
                    <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/7.jpg') }}"></a>
                    </li>
                    <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/8.jpg') }}"></a>
                    </li>
                    <li>
                        <a class="thumb" href="{{ asset('admin/slider/img/9.jpg') }}"></a>
                    </li>
                   -->
                </ul>
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
    <!--end-->