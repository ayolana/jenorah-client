<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('test', function () {
    return view('test');
});

Route::get('test-template', function () {
    return view('template');
});

Route::get('image-test', function(){
	// return view('test');
	$pi = 'zidaneIMG.jpg';
	$tst = Image::make( 'http://localhost/client-website/public/uploads/bestday1.png' );

		// // open an image file
		// // create a new empty image resource with transparent background
		// $img = Image::canvas(800, 600);

		// // create a new empty image resource with red background
		// $img = Image::canvas(32, 32, '#ff0000');
		$img = Image::make('http://localhost/client-website/public/uploads/bestday2.png');
		// crop the best fitting 5:3 (600x360) ratio and resize to 600x360 pixel
		$img->fit(300, 180);

		$img->save(env('fileupload').$pi );
});


Route::match(['get'], '/', ['uses' => 'HomeController@Homepage', 'as' => 'home']);
Route::match(['get', 'post'], '/user-details', ['uses' => 'HomeController@UserDetails', 'as' => 'user-details']);
Route::match(['get', 'post'], '/user-event/{slug}', ['uses' => 'HomeController@CoupleEvent', 'as' => 'user-event']);
Route::match(['get', 'post'], '/user-gallery/{slug}', ['uses' => 'HomeController@CoupleGallery', 'as' => 'user-gallery']);
Route::match(['get', 'post'], '/user-template/{slug}', ['uses' => 'HomeController@CoupleTemplate', 'as' => 'user-template']);
Route::match(['get', 'post'], '/user/pictures/{slug}', ['uses' => 'HomeController@CoupleOthers', 'as' => 'user-others']);
Route::match(['get', 'post'], '/get-template', ['uses' => 'HomeController@GetTemplate', 'as' => 'get-templatsse']);
Route::match(['get', 'post'], '/weddings/{id}', ['uses' => 'HomeController@ViewTemplate', 'as' => 'view-template']);
Route::match(['get', 'post'], '/wedding/{slug}', ['uses' => 'HomeController@CoupleView', 'as' => 'couple-view']);


Route::match(['get', 'post'], '/add-details', ['uses' => 'CouplesController@BasicDetails', 'as' => 'basic-details']);

// BACKEND

Route::group(['middleware' => ['auth']], function () {

// Route::auth();


Route::match(['get', 'post'], '/backend/dashboard', ['uses' => 'BackendController@CoupleDashboard', 'as' => 'dashboard']);
Route::match(['get', 'post'], '/backend/add', ['uses' => 'BackendController@CoupleDetails', 'as' => 'user-add']);
Route::match(['get', 'post'], '/backend/stage-two', ['uses' => 'BackendController@CoupleTwo', 'as' => 'user-two']);
Route::match(['get', 'post'], '/backend/stage-three', ['uses' => 'BackendController@CoupleThree', 'as' => 'user-three']);
Route::match(['get', 'post'], '/backend/couple-gallery', ['uses' => 'BackendController@CouplesGallery', 'as' => 'couple-gallery']);
Route::match(['get', 'post'], '/backend/select-template/{id}', ['uses' => 'BackendController@CouplesTemplate', 'as' => 'couple-template']);
Route::match(['get', 'post'], '/backend/get-template', ['uses' => 'BackendController@GetTemplate', 'as' => 'get-template']);
Route::match(['get', 'post'], '/backend/choose-template', ['uses' => 'BackendController@ChooseTemplate', 'as' => 'choose-template']);
Route::match(['get', 'post'], '/contact-us', ['uses' => 'HomeController@ContactForm', 'as' => 'contact-us']);

});




Route::get('temps/ilove', function () {
    return view('templates/ilove');
});

Route::get('temps/polo', function () {
    return view('templates/polo');
});

Route::get('temps/slide', function () {
    return view('templates/slide');
});

Route::get('temps/hotboot', function () {
    return view('templates/hotboot');
});

Route::get('temps/bestday', function () {
    return view('templates/bestday');
});


// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');