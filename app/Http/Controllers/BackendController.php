<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Couple;
use App\Models\Groom;
use App\Models\Bride;
use App\Models\Template;
use App\Models\CoupleSchedule;
use App\Models\CouplePicture;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;
use App\User;
use Validator;

class BackendController extends Controller
{
	public function CoupleDashboard(Request $request){

		$user = User::where('id', Auth::user()->id)->with('couple')->first();

		if(!$user->couple){
			return redirect()->route('user-add');
		}


		return view('backend.dashboard');
	}

	public function CoupleDetails(Request $request)
	{

		// dd(Auth::user());
		$user = Auth::user();

		if($request->isMethod('post')){
			// dd($request->request);

			// Groom Single
            $groom_sing  = str_ireplace(' ', '', $request->groom_single->getClientOriginalName());
            $date = (strtotime("now"));
            $groom_sing = $date.'-'.$groom_sing;
           
            $upload = $request->file('groom_single')->move(
                env('fileupload'), $groom_sing
            );

            $bride_sing  = str_ireplace(' ', '', $request->bride_single->getClientOriginalName());
            $date = (strtotime("now"));
            $bride_sing = $date.'-'.$bride_sing;

            $upload = $request->file('bride_single')->move(
                env('fileupload'), $bride_sing
            );

            
            // dd('Uploaded');

			$slug = str_slug($request->bride_dn.' and '.$request->groom_dn);

			$user_id = $user->id;
			$d = array();
			$d['marriage_date'] = date('Y-m-d', strtotime($request->marriage_date));
			$d['our_love_story'] = $request->love_story;
			$d['origin_of_love'] = $request->love_origin;
			$d['user_id'] = $user_id;
			$d['wedding_location'] = $request->location;
			$d['slug'] = $slug;
			
			$couple = Couple::create($d);

			// dd($couple);

			$groom = new Groom;
	        $groom->couple_id = $couple->id;
	        $groom->display_name = $request->groom_dn;
	        $groom->full_name = $request->groom_fn;
	        $groom->about = $request->groom_about;
	        $groom->toast = $request->groom_toast;
	        $groom->picture = $groom_sing;
	        $groom->save();

	        


	        $bride = new Bride;
	        $bride->couple_id = $couple->id;
	        $bride->display_name = $request->bride_dn;
	        $bride->full_name = $request->bride_fn;
	        $bride->about = $request->bride_about;
	        $bride->toast = $request->bride_toast;
	        $bride->picture = $bride_sing;
	        $bride->save();


	        return redirect()->route('user-two');


		}

		return view('backend.details2');
	}



	public function CoupleTwo(Request $request)
	{
		
		$user = Auth::user();
		$couple = Couple::where('user_id', $user->id)->first();
		// dd($couple);

		if($request->isMethod('post')){
			$t = [];
			for ($i=1; $i < 4; $i++) { 
				$date = 'date'.$i;
				$time = 'time'.$i;
				$location = 'location'.$i;
				$info = 'info'.$i;

				switch ($i) {
					case '1':
						$name = 'Engagement';
						break;
					case '2':
						$name = 'Wedding';
						break;
					default:
						$name = 'Reception';
						break;
				}

				if(!empty($request->$location)){

					$t['couple_id'] = $couple->id;
					$t['name'] = $name;
					$t['date'] = $request->$date;
					$t['time'] = $request->$time;
					$t['info'] = $request->$info;
					$t['location'] = $request->$location;
					
					CoupleSchedule::create($t);
				}
			}

			return redirect()->route('user-three');
		}

		return view('backend.stage_two');

	}

	public function CoupleThree(Request $request)
	{
		$user = Auth::user();
		// dd($user)
		$couple = Couple::where('user_id', $user->id)->first();
		$couple_id = $couple->id;
		

		return view('backend.stage_three', compact('couple_id'));

		
	}

	public function CouplesGallery(Request $request)
	{
		// dd($request->request);
		$user = Auth::user();
		// dd($user)
		$couple = Couple::where('user_id', $user->id)->first();
		$couple_id = $couple->id;


		if($request->isMethod('post')){

			if($request->type == 'couples' ){
				// getting all of the post data
			    $files = $request->file('couples');
			    // Making counting of uploaded images
			    $file_count = count($files);   

				// $uploadcount = 0;
			    foreach($files as $file) {
			      $rules = array('couples' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			      $validator = Validator::make(array('couples'=> $file), $rules);
			      if($validator->passes()){

			      	// dd('Here');
			        $destinationPath = env('fileupload');
			        $filename = str_ireplace(' ', '', $file->getClientOriginalName());

			        $date = (strtotime("+6hours"));
	           		$file_name = $date.'-'.$filename;
			        $upload_success = $file->move($destinationPath, $file_name);
			        // $uploadcount ++;
			      }

			      	CouplePicture::create([
		            	'name' => 'couples',
		            	'picture' => $file_name,
						'couple_id' => $couple_id,
						'created_at' => date('Y-m-d H:i:s')
	            	]);

			    }
			}else{
				// getting all of the post data
			    $files = $request->file('gallery');
			    // Making counting of uploaded images
			    $file_count = count($files); 
				// dd($file_count);

				foreach($files as $file) {
			      $rules = array('couples' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			      $validator = Validator::make(array('couples'=> $file), $rules);
			      if($validator->passes()){

			      	// dd('Here');
			        $destinationPath = env('fileupload');
			        $filename = str_ireplace(' ', '', $file->getClientOriginalName());

			        $date = (strtotime("+6hours"));
	           		$file_name = $date.'-'.$filename;
			        $upload_success = $file->move($destinationPath, $file_name);
			        // $uploadcount ++;
			      }

			      	CouplePicture::create([
		            	'name' => 'gallery',
		            	'picture' => $file_name,
						'couple_id' => $couple_id,
						'created_at' => date('Y-m-d H:i:s')
	            	]);

			    }

			}

		    // dd('Fantastic Upload');
		}
	}



	public function CouplesTemplate(Request $request, $couple_id)
	{
		// Getting Templates
		$templates = Template::with('pics')->get();
		$temp = Template::with('pics')->where('id', $couple_id)->first();

		// dd($template->toArray());
		return view('backend.stage_template', compact('temp', 'templates'));

	}

	public function ChooseTemplate(Request $request)
	{	
		$user = Auth::user();
		$couple = Couple::where('user_id', $user->id)->first();
		$couple_id = $couple->id;

		$couple = Couple::find($couple_id);

		$couple->update(['template_id' => $request->template_id]);
       
       	echo route('couple-view', ['slug' => $couple->slug]);		   
		// dd($request->template_id);
	}

	
}




?>