<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Couple;
use App\Models\Template;
use App\Models\CoupleSchedule;
use App\Models\CoupleGallery;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;
use Validator;
use DB;

class HomeController extends Controller
{

	public function Homepage(Request $request){

		return view('home.index');
	}


	public function UserDetails(Request $request)
	{

		if($request->isMethod('post')){
			// dd($request->request);

			$this->validate($request, [
		        'groom_full_name' => 'required',
		        'bride_full_name' => 'required',
		        'groom_display_name' => 'required',
		        'bride_display_name' => 'required',
		        'groom_toast' => 'required',
		        'bride_toast' => 'required',
		        'our_love_story' => 'required',
		        'origin_of_love' => 'required',
		    ]);

			// if ($validator->fails()) {
	  //           return redirect()->back()->withErrors($validator)->withInput();
   //      	}

		    $couple = Couple::FirstorCreate([
		    	'slug' => str_slug($request->bride_display_name.' '.$request->groom_display_name),
		    	'groom_fullname' => $request->groom_full_name,
		    	'bride_fullname' => $request->bride_full_name,
		    	'groom_display_name' => $request->groom_display_name,
		    	'bride_display_name' => $request->bride_display_name,
		    	'groom_toast' => $request->groom_toast,
		    	'bride_toast' => $request->bride_toast,
		    	'love_story' => $request->our_love_story,
		    	'origin_of_love' => $request->origin_of_love,
		    	'published' => 1,
		    	'user_id' => Auth::user()->id
		    ]);

		    // dd($couple);

		    return redirect()->route('user-event', ['id' => $couple->id]);

		}

		return view('users.details');

	}

	public function CoupleEvent(Request $request, $couple_id)
	{
		// dd($slug);
		if($request->isMethod('post')){

			// dd($request->request);
			if(!empty($request->engagement_date)){
				CoupleSchedule::FirstorCreate([
					'name' => $request->engagement_name,
					'date' => $request->engagement_date,
					'time' => $request->engagement_time,
					'info' => $request->engagement_info,
					'couple_id' => $couple_id,
				]);
			}

			if(!empty($request->wedding_date)){
				CoupleSchedule::FirstorCreate([
					'name' => $request->wedding_name,
					'date' => $request->wedding_date,
					'time' => $request->wedding_time,
					'info' => $request->wedding_info,
					'couple_id' => $couple_id,
				]);
			}

			if(!empty($request->reception_date)){
				CoupleSchedule::FirstorCreate([
					'name' => $request->reception_name,
					'date' => $request->reception_date,
					'time' => $request->reception_time,
					'info' => $request->reception_info,
					'couple_id' => $couple_id,
				]);
			}

			return redirect()->route('user-gallery', ['id' => $couple_id]);

		}
		
		return view('users.event');

	}

	public function CoupleGallery(Request $request, $couple_id)
	{
		// dd($slug);
		
		if ($request->isMethod('post')) {

			// getting all of the post data
		    $files = $request->file('gallery');
		    // Making counting of uploaded images
		    $file_count = count($files);
		    // start count how many uploaded
		    	
		    	// dd($files);
		    $uploadcount = 0;
		    foreach($files as $file) {
		      $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
		      $validator = Validator::make(array('file'=> $file), $rules);
		      if($validator->passes()){
		        $destinationPath = env('fileupload');
		        $filename = $file->getClientOriginalName();
		        $date = (strtotime("+6hours"));
           		$file_name = $date.'-'.$filename;
		        $upload_success = $file->move($destinationPath, $file_name);
		        $uploadcount ++;
		      }

		      CoupleGallery::create([
            	'name' => 'gallery',
            	'picture' => $file_name,
				'couple_id' => $couple_id,
            ]);

		    }

			
			$this->validate($request, [
		        'groom_single' => 'required',
		        //'bride_single' => 'required',
		        'couple1' 	   => 'required',
		        'couple2' 	   => 'required',
		        'couple3' 	   => 'required',
		    ]);


			$ext =  $request->file('groom_single')->getClientOriginalExtension();  
            $file_name  = ($request->groom_single->getClientOriginalName());
            $date = (strtotime("now"));
            $groom_single = $date.'-'.$file_name;

            $upload = $request->file('groom_single')->move(
                env('fileupload'), $groom_single
            );

            CoupleGallery::create([
            	'name' => 'groom_single',
				'couple_id' => $couple_id,
            	'picture' => $groom_single,
            ]);

            $ext =  $request->file('bride_single')->getClientOriginalExtension();  
            $file_name  = ($request->bride_single->getClientOriginalName());
            $date = (strtotime("+1hour"));
            $bride_single = $date.'-'.$file_name;

            $upload = $request->file('bride_single')->move(
                env('fileupload'), $bride_single
            );


            CoupleGallery::create([
            	'name' => 'bride_single',
				'couple_id' => $couple_id,
            	'picture' => $bride_single,
            ]);

            $ext =  $request->file('couple1')->getClientOriginalExtension();  
            $file_name  = ($request->couple1->getClientOriginalName());
            $date = (strtotime("+1hour"));
            $couple1 = $date.'-'.$file_name;

            $upload = $request->file('couple1')->move(
                env('fileupload'), $couple1
            );

            CoupleGallery::create([
            	'name' => 'couple1',
				'couple_id' => $couple_id,
            	'picture' => $couple1,
            ]);

            $ext =  $request->file('couple2')->getClientOriginalExtension();  
            $file_name  = ($request->couple2->getClientOriginalName());
            $date = (strtotime("+1hour"));
            $couple2 = $date.'-'.$file_name;

            $upload = $request->file('couple2')->move(
                env('fileupload'), $couple2
            );

            CoupleGallery::create([
            	'name' => 'couple2',
				'couple_id' => $couple_id,
            	'picture' => $couple2,
            ]);

            $ext =  $request->file('couple3')->getClientOriginalExtension();  
            $file_name  = ($request->couple3->getClientOriginalName());
            $date = (strtotime("+1hour"));
            $couple3 = $date.'-'.$file_name;

            $upload = $request->file('couple3')->move(
                env('fileupload'), $couple3
            );

            CoupleGallery::create([
            	'name' => 'couple3',
				'couple_id' => $couple_id,
            	'picture' => $couple3,
            ]);
            // $flight = App\Flight::create(['name' => 'Flight 10']);

			return redirect()->route('user-template', ['id' => $couple_id]);

		}

		return view('users.choose_gallery');

	}


	public function CoupleTemplate(Request $request, $couple_id){

		$templates = Template::get();
		$template = Template::with('pics')->where('id', $couple_id)->first();

		return view('users.choose_templates', compact('templates', 'template'));
	}

	public function GetTemplate(Request $request)
	{
		// dd($request->request);
		$template = Template::with('pics')->where('id', $request->template_id)->first();
		$templates = Template::get();
		
		return view('users.get_template', compact('templates', 'template'));

	}


	public function ViewTemplate(Request $request, $id)
	{	
		$couple = Couple::where('id', 5)
          ->update(['template_id' => 5]);
		
	}

	public function CoupleView(Request $request, $slug)
	{
		$couple = Couple::with('template', 'groom', 'bride', 'galleries', 'schedules', 'user')->where('slug', $slug)->first();
		$page_title = $couple->groom_display_name.' '.$couple->bride_display_name;
		$couplePix =array();
		$galleryPixs =array();
		
		foreach ($couple->galleries as $gallery) {

			switch ($gallery->name) {
				case 'groom_single':
				$groomPix =	$gallery->picture;
					break;
				case 'bride_single':
				$bridePix =	$gallery->picture;
					break;
				case 'couple':
				$couplePix[] =	$gallery->picture;
					break;

				case 'gallery':
				$galleryPixs[] =	$gallery->picture;
					break;

				default:
					# code...
					break;
			}
		}

		// dd($galleryPixs);
		// dd($couple->toArray());
		// dd(env('picdisplay').$groomPix);
		return view('templates.'.$couple->template->name, compact('couple', 'page_title', 'groomPix', 'bridePix', 'couplePix', 'galleryPixs'));

	}

	public function ContactForm(Request $request)
	{	


		DB::table('couple_contacts')->insert(
		    ['couple_id' => $request->couple_id, 'guests'=>$request->guests, 'name' => $request->name, 'phone' => $request->phone, 'email' => $request->email, 'comment' => $request->comments]
		);
		// dd($request->request);

		echo 'true';
	}

}




?>