<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Couple extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    // protected $table = 'menu_items';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['groom_fullname', 'wedding_location', 'template_id', 'slug', 'user_id', 'marriage_date','our_love_story', 'origin_of_love', 'published'];

    public function template(){
        return $this->belongsTo('App\Models\Template');
    }

     public function galleries(){
        return $this->hasMany('App\Models\CouplePicture');
    }

     public function schedules(){
        return $this->hasMany('App\Models\CoupleSchedule');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function groom(){
        return $this->hasOne('App\Models\Groom');
    }

    public function bride(){
        return $this->hasOne('App\Models\Bride');

    }
    

}
