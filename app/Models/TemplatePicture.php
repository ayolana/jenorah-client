<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemplatePicture extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'template_pictures';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'color'];


}
