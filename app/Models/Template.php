<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    // protected $table = 'menu_items';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'color'];

    public function pics(){

        return $this->hasMany('App\Models\TemplatePicture');
    }


}
