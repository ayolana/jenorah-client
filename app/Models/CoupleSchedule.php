<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoupleSchedule extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    // protected $table = 'menu_items';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'couple_id', 'date', 'time', 'info', 'location'];

    public $timestamps = false;

}
